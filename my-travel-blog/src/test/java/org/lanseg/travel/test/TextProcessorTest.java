package org.lanseg.travel.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Test;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Trip;
import org.lanseg.travel.text.Processor;
import static org.junit.Assert.*;
import org.junit.Before;
import org.lanseg.travel.text.Tag;
import org.lanseg.travel.text.TripContext;

/**
 *
 * @author lans
 */
public class TextProcessorTest {

    private Record r;
    private Processor p;

    @Before
    public void preparePost() {
        Trip t = new Trip();
        t.setId("trip_id");
        r = new Record();
        r.setTripId("trip_2020");
        r.setId(100500);
        r.setBody("Hello, [img 3]");
        List<Picture> pictures = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Picture a = new Picture();
            a.setId(i);
            a.setPath("/what/ever");
            a.setTitle("Image title here");
            pictures.add(a);
        }
        p = new Processor(new TripContext(t, Collections.singletonList(r), pictures, Collections.EMPTY_LIST),
                true);
    }

    @Test
    public void basicTest() {
        p.process();
        Tag testTag = new Tag("[img 3]");
        assertEquals(String.format("Hello, <div class=\"img-box\"><div class=\"content\">"
                + "<div class=\"gallery_item active\">"
                + "<a data-lightbox=\"%s\" href=\"/files/trip_id//what/ever\">"
                + "<img alt=\"Image title here\" data-src=\"/files/trip_id//what/ever\" />"
                + "<div class=\"data-title\"></div>"
                + "</a>"
                + "<div class=\"caption\">Image title here</div></div></div></div>",
                Long.toHexString((long) testTag.hashCode()), 0), r.getBody());
    }

    @Test
    public void incorrectPictureIdTest() {
        r.setBody("Hello, [img 3error]");
        p.process();
        assertEquals("Hello, <i>No such item</i>", r.getBody());
    }

    @Test
    public void unrelatedClosingTagTest() {
        r.setBody("[/point 123]");
        p.process();
        assertEquals("<span class='tag_error'>/point</span>", r.getBody());
    }
}
