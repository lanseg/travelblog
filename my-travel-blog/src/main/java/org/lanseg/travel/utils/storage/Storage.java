package org.lanseg.travel.utils.storage;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.utils.ImageTransformUtil;
import org.lanseg.travel.utils.Thumbnail;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lans
 */
public class Storage {

    private static final Logger LOG = Logger.getLogger("Storage");
    private static final int THUMBNAILER_THREADS = 1;
    private final String root;
    private final ExecutorService tasks = Executors.newFixedThreadPool(THUMBNAILER_THREADS);
    private final FileUtil files;

    public Storage(String root) {
        this.root = root;
        files = new FileUtil(root);
    }

    public void initTrip(String tripId) {
        try {
            Files.createDirectories(Paths.get(root, tripId, "photos"));
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Unable to create directory for trip: {0}", ex.getMessage());
        }
    }
    
    public void uploadForTrip(String tripId, String filename, InputStream in) {
        tasks.execute(() -> {
            try {
                String result = files.uploadForTrip(tripId, filename, in);
                LOG.log(Level.INFO, "Background updated: {0}", result);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Background upload failed: {0}", ex.getMessage());
            }
        });
    }

    public void removeForTrip(String tripId, String file) {
        tasks.execute(() -> {
            try {
                files.removeForTrip(tripId, file);
                LOG.log(Level.INFO, "Track removed for: {0}", tripId);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Track remove failed: {0}", ex.getMessage());
            }
        });
    }

    public Future<List<Picture>> uploadPhotos(String tripId, String shortcut, int recordId,
            List<MultipartFile> toUpload) {
        return tasks.submit(() -> {
            try {
                List<Picture> pictures = new ArrayList<>(toUpload.size());
                for (MultipartFile file : toUpload) {
                    String result = files.upload(tripId, shortcut, file.getOriginalFilename(), file.getInputStream());
                    String path = files.getAbsolutePath(Paths.get(tripId, result).toString());
                    Thumbnail big = ImageTransformUtil.scaleToSize(path, 1280, 1024);
                    Thumbnail small = ImageTransformUtil.scaleToSize(path, 160, 160);
                    Picture a = new Picture();
                    a.setPath(getRelativePath(tripId, big.getThumbnailPath()));
                    a.setThumbnail(getRelativePath(tripId, small.getThumbnailPath()));
                    a.setRecordId(recordId);
                    pictures.add(a);
                }
                return pictures;
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Image resize failed: {0}", ex.getMessage());
            }
            return Collections.EMPTY_LIST;
        });
    }

    public void rotatePicture(String tripId, Picture picture) {
        tasks.execute(() -> {
            try {
                files.rotatePicture(tripId, picture);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Image rotation failed: {1} ({0})",
                        new Object[]{picture, ex.getMessage()});
            }
        });
    }

    public String getRoot() {
        return root;
    }
    
    private String getRelativePath(String tripId, String path) {
        return files.getRelativePath(path).replace(tripId, "");
    }
    
    public boolean fileExists(String name) {
        return files.fileExists(name);
    }
}
