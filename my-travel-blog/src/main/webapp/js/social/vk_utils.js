/*jslint browser: true*/
/*jslint white: true */
/*global $, VK, gui */
(function (root) {
    "use strict";
    root.social = root.social || {};

    root.social.vk = {
        version: '5.92',
        userId: null,
        initCalled: false,
        updateAuthors: function () {
            var authorNodes = {}, authors = [];
            $(".data-title .author").each(function (obj) {
                if (isNaN(this.getAttribute('data-userid'))) {
                    return;
                }
                var authorId = parseInt(this.getAttribute('data-userid'));
                if (authorNodes[authorId] === undefined) {
                    authorNodes[authorId] = [];
                }
                authorNodes[authorId].push(this);
                if (authors.indexOf(authorId) === -1) {
                    authors.push(authorId);
                }
            });
            this.getInfo(function (info) {
                var user, nodes, href;
                if (!Array.isArray(info)) {
                    info = [info];
                }
                for (var i = 0; i < info.length; i++) {
                    user = info[i];
                    if (user.uid in authorNodes) {
                        nodes = authorNodes[user.uid];
                        for (var j = 0; j < nodes.length; j++) {
                            href = document.createElement("a");
                            href.setAttribute("href", "http://vk.com/id" + user.uid);
                            href.textContent = user['first_name'] + ' ' + user['last_name'];
                            nodes[j].appendChild(href);
                        }
                    }
                }
            }, authors);
        },
        getInfo: function (callback, id) {
            var params = {fields: ["photo_50"], v: this.version};
            if (arguments.length === 2) {
                params["user_ids"] = id;
            }
            VK.Api.call('users.get', params, function (r) {
                if (r.response) {
                    callback(r.response[0]);
                }
            });
        },
        getAlbums: function (callback, id) {
            VK.Api.call('photos.getAlbums', {
                owner_id: id === undefined ? this.userId : id,
                v: this.version
            },
                    function (r) {
                        if (r.response) {
                            callback(r.response);
                        }
                    });
        },
        getPhotos: function (album, owner, callback) {
            VK.Api.call('photos.get', {album_id: album, owner_id: owner, count: 1000, rev: 1, v: this.version},
                    function (r) {
                        if (r.response) {
                            console.log(r.response);
                            callback(r.response);
                        }
                    });
        },
        checkPermissions: function (callback) {
            VK.Api.call('account.getAppPermissions', {user_id: this.userId, v: this.version},
                    function (r) {
                        if (r.response) {
                            callback(r.response);
                        }
                    });
        },
        initPhotoChooser: function (userId) {
            var user = document.getElementById("vk_user"), that = this;
            user.textContent = userId;
            this.userId = userId;
            this.getInfo(function (info) {
                var img = document.createElement('img'),
                        username = document.createElement('span');
                img.setAttribute('src', info['photo_50']);
                username.textContent = info['first_name'] + " " +
                        info['last_name'] + ", " + info["id"];
                user.textContent = '';
                user.appendChild(username);
            }, [userId]);
            this.getAlbums(function (albums) {
                function createOption(value, text, blocked) {
                    var opt = document.createElement('option');
                    opt.value = value;
                    opt.innerHTML = text;
                    if (blocked) {
                        opt.selected = true;
                        opt.disabled = true;
                    }
                    return opt;
                }
                var i, select = document.getElementById('albums_select');
                while (select.length) {
                    select.remove(0);
                }
                select.appendChild(createOption('', '...', true));
                select.appendChild(createOption('wall', 'Стена'));
                select.appendChild(createOption('profile', 'Профиль'));
                select.appendChild(createOption('saved', 'Сохранённые'));
                for (i = 0; i < albums["count"]; i++) {
                    var album = albums["items"][i];
                    select.appendChild(createOption(album.id, album.title));
                }
                select.addEventListener('change', function (e) {
                    that.getPhotos(select.value, userId, gui.photos.showPhotos);
                });
            }, userId);
        },
        onAuth: function (response) {
            if (response.session) {
                this.initPhotoChooser(response.session.mid);
            } else {
                user.textContent = "no_auth";
            }
        },
        initComments: function (el) {
            this.init(true);
            if (el.innerHTML === '') {
                VK.Widgets.Comments(el.id, {
                    autoPublish: 0
                }, el.id.replace('vk_comments_', ''));
            }
        },
        init: function (anonOk) {
            var that = this;
            if (!this.initCalled) {
                this.initCalled = true;
                VK.init({
                    apiId: 4847387
                });
            }
            if (!anonOk) {
                VK.Auth.getLoginStatus(function (response) {
                    if (response.session) {
                        that.onAuth(response);
                    } else {
                        VK.Auth.login(that.onAuth, 4);
                    }
                });
            }
        }
    };
})(this);