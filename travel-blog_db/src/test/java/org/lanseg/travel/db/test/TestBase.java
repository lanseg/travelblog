package org.lanseg.travel.db.test;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.lanseg.travel.orm.RecordMapper;
import org.lanseg.travel.orm.TripMapper;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Trip;

/**
 *
 * @author lans
 */
public class TestBase {

    private static final String CONFIG = "org/lanseg/travel/orm/config.xml";
    protected SqlSession session;

    protected TripMapper tripMapper;
    protected RecordMapper recordMapper;

    protected Trip testTrip;
    protected Record testRecord;
    protected Record publishedRecord;

    public TestBase() {
        try {
            session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream(CONFIG)).openSession(true);
        } catch (IOException ex) {
            //ex.printStackTrace();
        }
        tripMapper = session.getMapper(TripMapper.class);
        recordMapper = session.getMapper(RecordMapper.class);

    }

    @Before
    public void setUp() {
        testTrip = new Trip(UUID.randomUUID().toString(), "Full trip title",
                "Test trip", "Whatever", new Date(), new Date());
        testRecord = new Record(10100 + (int) (Math.random() * 10000), testTrip.getId(),
                "short_cut_" + Math.random(), "Test record",
                "Whatever", "Whatever", new Date(), true);
        publishedRecord = new Record(testRecord.getId() + 1, testTrip.getId(),
                "short_cut_" + Math.random() + "_published", "Test record (published)",
                "Whatever (published)", "Whatever (published)", new Date(), false);
        tripMapper.addTrip(testTrip);
        recordMapper.addRecord(publishedRecord);
        recordMapper.addRecord(testRecord);
    }

    @After
    public void tearDown() {
        recordMapper.deleteRecord(testRecord.getId());
        recordMapper.deleteRecord(publishedRecord.getId());
        tripMapper.deleteTrip(testTrip.getId());

    }
}
