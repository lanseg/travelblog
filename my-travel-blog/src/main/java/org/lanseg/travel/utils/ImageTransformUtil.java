package org.lanseg.travel.utils;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author lans
 */
public class ImageTransformUtil {

    public static void turn(String path) throws IOException {
        File file = new File(path);
        BufferedImage src = ImageIO.read(file);
        AffineTransformOp transform = new AffineTransformOp(
                AffineTransform.getRotateInstance(Math.PI / 2,
                        src.getHeight() / 2, src.getHeight() / 2), AffineTransformOp.TYPE_BILINEAR);
        BufferedImage dst = new BufferedImage(src.getHeight(), src.getWidth(), src.getType());
        ImageIO.write(transform.filter(src, dst), Utils.getExtension(path).toUpperCase(), file);
    }

    private static Dimension getScaledSize(int width, int height,
            int desiredWidth, int desiredHeight) {
        double k = Math.min((double) desiredWidth / width, (double) desiredHeight / height);
        return new Dimension((int) (width * k), (int) (height * k));
    }

    public static Thumbnail scaleToSize(String path, int width, int height) throws IOException {
        BufferedImage source = ImageIO.read(new File(path));
        if (source.getHeight() < height && source.getWidth() < width) {
            return new Thumbnail(path, path, width, height);
        }
        Dimension newSize = getScaledSize(source.getWidth(), source.getHeight(), width, height);
        Image scaled = source.getScaledInstance(newSize.width, newSize.height, BufferedImage.SCALE_SMOOTH);
        BufferedImage result = new BufferedImage(newSize.width, newSize.height, source.getType());
        result.getGraphics().drawImage(scaled, 0, 0, null);
        String extension = path.substring(path.lastIndexOf(".") + 1);
        String filename = path.substring(0, path.lastIndexOf("."));
        String dest = String.format("%s_thumb%dx%d.%s", filename, width, height, extension);
        ImageIO.write(result, extension, new File(dest));
        return new Thumbnail(dest, path, newSize.width, newSize.height);
    }
}
