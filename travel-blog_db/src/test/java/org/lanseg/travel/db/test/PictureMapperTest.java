package org.lanseg.travel.db.test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.lanseg.travel.orm.PictureMapper;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.PictureInfo;

/**
 *
 * @author lans
 */
public class PictureMapperTest extends TestBase {

    private final PictureMapper pictureMapper;

    public PictureMapperTest() {
        pictureMapper = session.getMapper(PictureMapper.class);
    }

    @Test
    public void testInsertRemove() {
        int recordsToTest = 10;
        List<Picture> attachments = new ArrayList<>();
        for (int i = 0; i < recordsToTest; i++) {
            Picture a = new Picture(
                    0, recordsToTest - i, testRecord.getId(),
                    "Picture #" + i, "Path #" + i, "Thumb #" + i);
            pictureMapper.addPicture(a);
            Picture fromBase = pictureMapper.getPicture(a.getId());
            assertEquals("Same from base", a, fromBase);
            attachments.add(a);
        }
        int addedAmount = pictureMapper.getPictureNumber(testRecord.getId());
        assertEquals(attachments, pictureMapper.getPictures(testRecord.getId()));
        attachments.forEach((item) -> pictureMapper.deletePicture(item.getId()));
        assertEquals("All removed ", 0, pictureMapper.getPictures(testRecord.getId()).size());
        assertEquals("Amount added", recordsToTest, addedAmount);
        assertEquals("All removed", 0, pictureMapper.getPictureNumber(testRecord.getId()));
    }

    @Test
    public void testUpdate() {
        Picture a = new Picture(0, 0, testRecord.getId(),
                "Picture #", "Path #", "Thumb #");
        pictureMapper.addPicture(a);
        a.setTitle("New title");
        a.setPath("New path");
        a.setThumbnail("New Thumbnail");
        pictureMapper.updatePicture(a);
        assertEquals(a, pictureMapper.getPicture(a.getId()));
        pictureMapper.deletePicture(a.getId());
    }

    @Test
    public void pictureInfoAddRemove() {
        Picture a = new Picture(0, 0, testRecord.getId(), "Picture #", "Path #", "Thumb #");
        pictureMapper.addPicture(a);
        PictureInfo pi = new PictureInfo(0, a.getId(), 123123L);
        pictureMapper.addPictureInfo(pi);
        Picture withInfo = pictureMapper.getPicture(a.getId());
        pictureMapper.deletePictureInfo(pi.getId());
        pictureMapper.deletePicture(a.getId());
        assertEquals(pi, withInfo.getInfo());
    }

    @Test
    public void updateOrderTest() {
        int toAdd = 10;
        List<Picture> original = new ArrayList<>(toAdd);
        for (int i = 0; i < toAdd; i++) {
            Picture p = new Picture(0, i, testRecord.getId(),
                    "Picture #" + i, "Path #" + i, "Thumb #" + i);
            p.setSortOrder(i);
            original.add(p);
            pictureMapper.addPicture(p);
        }
        Picture source = original.get(3);
        Picture target = original.get(8);
        pictureMapper.updateOrder(testRecord.getId(), source.getId(), source.getSortOrder(), target.getSortOrder());
        List<Picture> afterMove = pictureMapper.getPictures(testRecord.getId());
        original.forEach(p -> pictureMapper.deletePicture(p.getId()));
        assertEquals(afterMove.get(8).getId(), source.getId());
    }
}
