package org.lanseg.travel.config;

import javax.sql.DataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author lans
 */
@Configuration
@MapperScan("org.lanseg.travel.orm")
@PropertySource("classpath:travel-environment.properties")
public class DataConfig {
    
    @Value("${db.username}")
    private String username;

    @Value("${db.password}")
    private String password; 

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setUrl("jdbc:mysql://localhost/travel?serverTimezone=Europe/Moscow");
        source.setDriverClassName("com.mysql.cj.jdbc.Driver");
        source.setUsername(username);
        source.setPassword(password);
        return source;
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactory() {
        SqlSessionFactoryBean session = new SqlSessionFactoryBean();
        session.setDataSource(dataSource());
        session.setTypeHandlersPackage("org.lanseg.travel.orm");
        return session;
    }
}
