(function (root) {
    "use strict";
    root.gui = root.gui || {};
    root.gui.scroll = {
        getTop: function (el) {
            var ly = 0;
            while (el !== null) {
                ly += el.offsetTop;
                el = el.offsetParent;
            }
            return ly;
        },
        init: function (nodeId, settings) {
            var getTop = this.getTop;
            $(document).scroll(function () {
                var links = document.getElementById(nodeId),
                        diary = $(".container.blog").get(0),
                        defaults, top, minTopOffset = 32, topOffset = 48;
                if (!links.hasOwnProperty("defaultPos")) {
                    links.defaultPos = {
                        width: links.getBoundingClientRect().width,
                        top: getTop(links)
                    };
                    links.style.position = 'absolute';
                    links.style.width = links.defaultPos.width + 'px';
                }
                defaults = links.defaultPos;
                top = $(this).scrollTop();
                if (settings.currentPage === "diary" || settings.currentPage === "") {
                    settings.scroll = top;
                }
                top = (top > (defaults.top - minTopOffset) ?
                        top + topOffset : defaults.top);
                if (diary.clientHeight > top + links.clientHeight) {
                    links.style.top = top + 'px';
                }
            });
        }
    };
})(this);