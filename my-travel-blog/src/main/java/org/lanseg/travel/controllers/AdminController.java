package org.lanseg.travel.controllers;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import org.lanseg.travel.orm.PointMapper;
import org.lanseg.travel.orm.UserMapper;
import org.lanseg.travel.orm.dao.PointType;
import org.lanseg.travel.orm.dao.User;
import org.lanseg.travel.utils.storage.Storage;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author lans
 */
@Controller
@Secured("editor")
@RequestMapping("/admin/")
@PreAuthorize("hasAuthority('admin')")
public class AdminController {

    private static final Pattern NON_LETTER = Pattern.compile("[^a-zA-Z]+");

    @Resource
    private UserMapper users;
    @Resource
    private Storage storage;
    @Resource
    private PointMapper points;

    @RequestMapping("")
    public String main(ModelMap model) throws IOException, URISyntaxException {
        Map<Path, BasicFileAttributes> localFiles = new LinkedHashMap<>();
        AtomicLong totalSize = new AtomicLong(0);
        Files.walkFileTree(Paths.get(new URI(storage.getRoot())), new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path t, BasicFileAttributes bfa) throws IOException {
                localFiles.put(t, bfa);
                totalSize.addAndGet(bfa.size());
                return FileVisitResult.CONTINUE;
            }
        });
        model.addAttribute("users", users.getUsers());
        model.addAttribute("pointTypes", points.getPointTypes());
        model.addAttribute("storageRoot", storage.getRoot());
        model.addAttribute("localFiles", localFiles);
        model.addAttribute("localFilesSize", totalSize.get());
        return "admin";
    }

    @RequestMapping(value = "update_user", method = RequestMethod.POST)
    public @ResponseBody
    User update(
            @RequestParam("field") String field,
            @RequestParam("id") String id,
            @RequestParam("value") String value) {
        User user = users.getUser(id);
        if (user == null) {
            return null;
        }
        switch (field) {
            case "password":
                user.setPassword(value);
                break;
            case "roles":
                List<String> newRoles = Arrays.asList(NON_LETTER.split(value));
                updateRoles(id, newRoles, user.getRoles());
                break;
            case "username":
                user.setUsername(value);
                break;
        }
        users.updateUser(user);
        return users.getUser(id);
    }

    @RequestMapping(value = "add_user", method = RequestMethod.POST)
    public String addUser(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("roles") String roles) {
        String id = UUID.randomUUID().toString();
        users.addUser(new User(id, username, password));
        for (String role : NON_LETTER.split(roles)) {
            users.addRole(id, role);
        }
        return "redirect:/admin/";
    }

    @RequestMapping("remove_user")
    public String addUser(@RequestParam("id") String id) {
        users.deleteUser(id);
        return "redirect:/admin/";
    }

    @RequestMapping(value = "add_point_type", method = RequestMethod.POST)
    public String addPointType(@RequestParam("point_type") String type) {
        points.addPointType(new PointType(0, type));
        return "redirect:/admin/";
    }

    @RequestMapping("remove_point_type")
    public String removePointType(@RequestParam("id") int id) {
        points.deletePointType(id);
        return "redirect:/admin/";
    }

    private void updateRoles(String user, List<String> newRoles, List<String> oldRoles) {
        newRoles.stream().filter((s) -> !oldRoles.contains(s)).forEach((role) -> users.addRole(user, role));
        oldRoles.stream().filter((s) -> !newRoles.contains(s)).forEach((role) -> users.removeRole(user, role));
    }
}
