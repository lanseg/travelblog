package org.lanseg.travel.orm;

import java.util.List;
import org.lanseg.travel.orm.dao.Track;

/**
 *
 * @author lans
 */
public interface TrackMapper {

    void addTrack(Track t);
    void deleteTrack(int id);
    void updateTrack(Track t);
    Track getTrack(int id);
    List<Track> getTripTracks(String tripId);
    List<Track> getRecordTracks(int recordId);
}
