package org.lanseg.travel.config;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import org.lanseg.travel.orm.UserMapper;
import org.lanseg.travel.orm.dao.User;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = Logger.getLogger("AUTH");

    @Resource
    private UserMapper userMapper;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        User user = userMapper.getUserByCredentials(username, password);
        if (user == null) {
            LOG.log(Level.INFO, "Failed to authenticate user \"{0}\".", username);
            throw new BadCredentialsException("Bad username or password.");
        }
        LOG.log(Level.INFO, "Authenticated user \"{0}\".", username);
        List<GrantedAuthority> authorities = permissionsToAuthority(user.getRoles());
        return new UsernamePasswordAuthenticationToken(user, password, authorities);
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return arg0.equals(UsernamePasswordAuthenticationToken.class);
    }

    private static List<GrantedAuthority> permissionsToAuthority(List<String> permissions) {
        List<GrantedAuthority> authorities = new ArrayList<>(permissions.size());
        permissions.forEach(perm -> authorities.add(new SimpleGrantedAuthority(perm)));
        return authorities;
    }
}
