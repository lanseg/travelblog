package org.lanseg.travel.db.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import static org.junit.Assert.*;
import org.junit.Test;
import org.lanseg.travel.orm.UserMapper;
import org.lanseg.travel.orm.dao.User;

/**
 *
 * @author lans
 */
public class UserMapperTest {

    private static final String CONFIG = "org/lanseg/travel/orm/config.xml";
    private UserMapper userMapper;

    public UserMapperTest() {
        try {
            SqlSession session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream(CONFIG)).openSession(true);
            userMapper = session.getMapper(UserMapper.class);
        } catch (IOException ex) {
        }
    }

    @Test
    public void testBasicUserActions() {
        assertNotNull("Mapper should be initialized", userMapper);
        int usersBefore = userMapper.getUserCount();
        User userA = new User(UUID.randomUUID().toString(), "username", "password");
        User userB = new User(UUID.randomUUID().toString(), "username2", "password2");
        userMapper.addUser(userA);
        userMapper.addUser(userB);
        assertTrue(userMapper.userExists(userA.getUsername(), userA.getPassword()));
        assertTrue(userMapper.userExists(userB.getUsername(), userB.getPassword()));
        assertTrue(userMapper.userExists(userA.getUsername(), userA.getPassword()));
        assertEquals(usersBefore + 2, userMapper.getUserCount());
        assertEquals(userA, userMapper.getUser(userA.getId()));
        assertEquals(userA, userMapper.getUserByCredentials(userA.getUsername(), userA.getPassword()));
        assertEquals(userB, userMapper.getUser(userB.getId()));
        System.out.println(userMapper.getUsers());
        assertTrue(userMapper.getUsers().containsAll(Arrays.asList(userA, userB)));
        userMapper.deleteUser(userA.getId());
        userMapper.deleteUser(userB.getId());
        assertEquals(usersBefore, userMapper.getUserCount());
    }

    @Test
    public void testBasicRoleActions() {
        assertNotNull("Mapper should be initialized", userMapper);
        User testUser = new User(UUID.randomUUID().toString(), "username", "password");
        userMapper.addUser(testUser);
        assertEquals(Collections.<String>emptyList(), userMapper.getRoles(testUser.getId()));
        userMapper.addRole(testUser.getId(), "whatever");
        userMapper.addRole(testUser.getId(), "anyway");
        List<String> roles = userMapper.getRoles(testUser.getId());
        assertEquals("User should have two roles", 2, roles.size());
        testUser = userMapper.getUser(testUser.getId());
        assertEquals(roles, testUser.getRoles());
        assertTrue(roles.contains("whatever"));
        assertTrue(roles.contains("anyway"));
        userMapper.removeRole(testUser.getId(), "whatever");
        assertEquals("User should have two roles", 1, userMapper.getRoles(testUser.getId()).size());
        userMapper.removeRole(testUser.getId(), "anyway");
        assertEquals("User should have two roles", 0, userMapper.getRoles(testUser.getId()).size());
        userMapper.deleteUser(testUser.getId());
    }

    @Test
    public void testRolesByCredentials() {
        User testUser = new User(UUID.randomUUID().toString(), "username", "password");
        userMapper.addUser(testUser);
        userMapper.addRole(testUser.getId(), "whatever");
        userMapper.addRole(testUser.getId(), "anyway");
        testUser = userMapper.getUserByCredentials(testUser.getUsername(), testUser.getPassword());
        assertEquals(2, testUser.getRoles().size());
        assertEquals(testUser.getRoles(), userMapper.getRoles(testUser.getId()));
        userMapper.removeRole(testUser.getId(), "anyway");
        userMapper.removeRole(testUser.getId(), "whatever");
        userMapper.deleteUser(testUser.getId());
    }

    @Test
    public void testUpdateUser() {
        User testUser = new User(UUID.randomUUID().toString(), "username", "password");
        userMapper.addUser(testUser);
        testUser.setUsername("NotAnUser");
        testUser.setPassword("Whatever");
        userMapper.updateUser(testUser);
        assertEquals(testUser, userMapper.getUser(testUser.getId()));
        userMapper.deleteUser(testUser.getId());
    }
}
