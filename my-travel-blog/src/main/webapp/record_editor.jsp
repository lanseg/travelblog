<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@include file="/WEB-INF/jspf/editor_header.jspf" %>
<div class="header">
    <a href="delete"><s:message code="editor.record.delete_record" /></a>
</div>
<div class="content">
    <div class="row">
        <s:message code="editor.record.edit_day" />:
        <select onchange="location.href = '/${tripId}/' + this.value + '/edit';">
            <c:forEach var="rec" items="${allRecords}">
                <option value="${rec.shortcut}" <c:if test="${rec.shortcut == record.shortcut}">selected</c:if> >${rec.title}</option>
            </c:forEach>
        </select>
    </div>
    <form method="POST" action="/${record.tripId}/${record.shortcut}/save" class="editor">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
        <div class="row">
            <input type="text" name="shortcut" value="${record.shortcut}" />
        </div>
        <div class="row">
            <input type="text" name="title" value="${record.title}" />
        </div>
        <div class="row date">
            <input type="text" name="date" value="<f:formatDate pattern="dd.MM.yyyy" value="${record.date}"/>" />
        </div>
        <div class="row">
            <input type="text" name="description" value="${record.description}" />
        </div>
        <div class="row">
            <input type="checkbox" name="draft" <c:if test="${record.draft}">checked</c:if> /> <s:message code="editor.record.draft_toggle" />
            </div>        
            <div class="row charmap">
                <span>«</span>
                <span>»</span>
                <span>¥</span>
            </div>
            <div class="row">
                <textarea id="body" name="body">${record.body}</textarea>
        </div>
        <div class="row">
            <button>Save</button>
        </div>
    </form>
</div>
<div class="content">
    <h3><s:message code="editor.record.add_photos" /></h3>
    <a href="#" id="vk_user"></a>
    <select id="albums_select">
        <option disabled selected></option>
    </select>      
    <div class="pictures">
        <div id="selected_photos" class="selected_photos">
            <div>
                <button onclick="gui.photos.addPhotos('${record.shortcut}');"><s:message code="editor.record.add_selected" /></button>
                <input type="checkbox" id="add_info" name="add_info" /><label for="add_info"><s:message code="editor.record.author_toggle" /></label>
            </div>
        </div>            
    </div>
    <div class="pictures">
        <div id="remote_gallery" class="remote_gallery"></div>            
    </div>
    <a href="javascript:void(0)" class="header header_toggler"><s:message code="editor.record.upload_asfile" /></a>
    <div  class="page">
        <form action="upload?${_csrf.parameterName}=${_csrf.token}" method="post" enctype="multipart/form-data">
            <input type="file" name="file" multiple />
            <input type="submit" value="<s:message code="editor.record.upload" />" name="submit" />
        </form>
    </div>
</div>
<div class="content">
    <h3><s:message code="editor.record.photos" /></h3>
    <div class="pictures">
        <c:forEach var="picture" items="${pictures}">
            <div class="picture" id="picture_${picture.id}" draggable="true">
                <c:choose>
                    <c:when test="${fn:startsWith(picture.path, 'http')}">
                        <c:set var="thumb_url" value="${picture.thumbnail}" />
                        <c:set var="photo_url" value="${picture.path}" />
                    </c:when>
                    <c:otherwise>
                        <c:set var="thumb_url" value="/files/${tripId}/${picture.thumbnail}" />
                        <c:set var="photo_url" value="/files/${tripId}/${picture.path}" />                        
                    </c:otherwise>
                </c:choose>
                <div class="thumbnail">
                    ${picture.id}
                    <a href='${photo_url}'><img src="${thumb_url}" /></a>
                </div>
                <div class="title">
                    <div class="text"><a href="#">${empty picture.title ? "Без имени" : picture.title}</a></div>
                    <a href="/${record.tripId}/${record.shortcut}/edit_picture?id=${picture.id}&action=rotate">
                        <span class="glyphicon glyphicon-repeat"></span>                    
                    </a>
                    <a href="/${record.tripId}/${record.shortcut}/delete_picture?id=${picture.id}">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>              
                    <c:if test="${not empty picture.info}">
                        <br/>©: ${picture.info.authorId}
                        <br/>Show: <a class="toggler" href="/${record.tripId}/${record.shortcut}/edit_info?id=${picture.id}&action=show_author_toggle">${picture.info.isAuthorShowing()}</a>
                    </c:if>
                </div>    
                <div style="clear: both"></div>
            </div>
        </c:forEach>
    </div>
</div>        
<div class="content points">
    <h2 id="pictures"><s:message code="editor.record.points" /></h2>
    <div class="icon_box" id="icon_box">
        <c:forEach var="type" items="${pointTypes}">
            <span data-type="${type.id}" class="icon map_icons ${type.type}"></span>
        </c:forEach>
        <span class="icon map_icons">X</span>
    </div>
    <table class="table" id="points">
        <thead>
            <tr>
                <th>Id</th>
                <th>Trip</th>
                <th>RecordId</th>
                <th><s:message code="editor.record.point_type"/></th>
                <th><s:message code="editor.record.point_date" /></th>
                <th><s:message code="editor.record.point_caption" /></th>
                <th><s:message code="editor.record.lontitude" /></th>
                <th><s:message code="editor.record.latitude" /></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="point" items="${points}">
                <tr>
                    <td class="id">${point.id}</td>
                    <td class="tripId">${point.tripId}</td>
                    <td class="recordId">${point.recordId}</td>
                    <td>
                        <a href="javascript:void(0);" data-point="${point.id}" class="icon_chooser">
                            <c:if test="${point.type.type != null}" >
                                <span class="map_icons ${point.type.type}"></span>
                            </c:if>
                            <c:if test="${point.type.type == null}" >
                                <span>NULL</span>
                            </c:if>                            
                        </a>
                    </td>
                    <td class="time"><f:formatDate pattern="dd.MM.yyyy HH:mm" value="${point.time}"/></td>
                    <td class="title">${point.title}</td>
                    <td class="lon">${point.lon}</td>
                    <td class="lat">${point.lat}</td>
                    <td>
                        <a class="item-edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        <a class="item-remove" href="remove_point?id=${point.id}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        <a class="item-apply"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<script>
    social.vk.init();
</script>
<%@include file="/WEB-INF/jspf/editor_footer.jspf" %>