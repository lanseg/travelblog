package org.lanseg.travel.orm.dao;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author lans
 */
public class Point {

    private static final double EPS = 0.000001;

    private String id;
    private String tripId;
    private String title;
    private Integer recordId;
    private Date time;
    private double lon;
    private double lat;
    private PointType type;
    
    public Point() {
    }

    public Point(String id, Integer recordId, String tripId, String title, Date time, double lon, double lat, PointType type) {
        this.id = id;
        this.title = title;
        this.recordId = recordId;
        this.tripId = tripId;
        this.time = time;
        this.lon = lon;
        this.lat = lat;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }


    public PointType getType() {
        return type;
    }

    public void setType(PointType type) {
        this.type = type;
    }
    
    @Override
    public String toString() {
        return String.format("Point {id: %s, tripId: %s, recordId: %s, lon: %f, lat: %f, type: %s}", id, tripId, recordId, lon, lat, type);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point)) {
            return false;
        }
        Point p = (Point) o;
        return id.equals(p.id)
                && ((recordId == null && p.recordId == null) || recordId.equals(p.recordId))
                && ((tripId == null && p.tripId == null) || tripId.equals(p.tripId))
                && (Math.abs(p.lon - lon) < EPS)
                && (Math.abs(p.lat - lat) < EPS);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.tripId);
        return hash;
    }
}
