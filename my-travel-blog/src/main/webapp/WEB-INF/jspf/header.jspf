<%@page contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">                     
        <title><c:out value="${trip.shortTitle}" default="Мои путешествия" /></title>                
        <link rel="stylesheet" href="/css/style.min.css">                 
        <script>
            var csrfToken = "${_csrf.token}";
            var csrfHeader = "${_csrf.headerName}";
        </script>         
    </head>
    <body>
        <div class="blog-masthead">
            <div class="container">
                <nav class="blog-nav">
                    <div class="dropdown menu blog-nav-item  narrow_menu">
                        <a href="#" class="dropdown-toggle top_level" data-toggle="dropdown" role="button">
                            &nbsp;
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#diary" class="category"><s:message code="header.menu.diary" /></a></li>
                            <li><a href="#map" class="category"><s:message code="header.menu.map" /></a></li>
                            <li>
                                <c:if test="${trip != null}">
                                    <a class="blog-nav-item menu" href="/${trip.id}/photos/"><s:message code="header.menu.photos" /></a>
                                </c:if>
                                <c:if test="${trip == null}">
                                    <a class="blog-nav-item menu" href="/photos/"><s:message code="header.menu.photos" /></a>
                                </c:if>                                
                            </li>
                        </ul>                        
                    </div>
                    <div class="dropdown menu blog-nav-item">
                        <a href="#" class="dropdown-toggle top_level" data-toggle="dropdown" role="button">
                            <c:choose>
                                <c:when test="${empty trip}">
                                    <s:message code="header.menu.trips" />
                                </c:when>
                                <c:otherwise>
                                    ${trip.shortTitle}
                                </c:otherwise>
                            </c:choose>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu trips" role="menu">
                            <li><a href="/"><s:message code="header.menu.trips" /></a></li>                            
                            <li role="menuitem" class="divider"></li>                            
                                <c:forEach var="trip" items="${trips}">
                                <li>
                                    <a href="/${trip.id}/">
                                        <c:if test="${trip.draft}">                                            
                                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                                        </c:if>${trip.shortTitle}
                                    </a>
                                </li>
                            </c:forEach>
                            <sec:authorize access="hasAuthority('editor')">
                                <li role="menuitem" class="divider"></li>
                                <li><a href="/add_trip"><s:message code="header.menu.add" /></a></li>
                                </sec:authorize>
                        </ul>
                    </div>
                    <a class="blog-nav-item menu category active" id="diary_toggle" href="#diary" ><s:message code="header.menu.diary" /></a>
                    <a class="blog-nav-item menu category" id="map_toggle" href="#map"><s:message code="header.menu.map" /></a>
                    <c:if test="${trip != null}">
                        <a id="photos_page" class="blog-nav-item menu" href="/${trip.id}/photos/"><s:message code="header.menu.photos" /></a>
                    </c:if>
                    <c:if test="${trip == null}">
                        <a id="photos_page" class="blog-nav-item menu" href="/photos/"><s:message code="header.menu.photos" /></a>
                    </c:if>
                    <sec:authorize access="hasAuthority('admin')">
                        <a class="blog-nav-item menu admin" href="/admin/"><s:message code="header.menu.admin" /></a>
                    </sec:authorize>                                            
                    <div class="blog-nav-item menu navbar-right">                                     
                        <sec:authorize access="hasAuthority('ROLE_ANONYMOUS')">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><s:message code="header.login.login" /> <span class="caret"></span></a>
                            <div class="dropdown-menu login-menu" role="menu">
                                <div role="menuitem">
                                    <form name='f' action='/login' method='POST'>
                                        <input type='text' name='username' value='' />
                                        <input type='password' name='password'/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
                                        <button>Вход</button>
                                    </form>    
                                </div>
                            </div>
                        </sec:authorize>
                        <sec:authorize access="!hasAuthority('ROLE_ANONYMOUS')">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            ${pageContext.request.userPrincipal.principal.username} (<a href="/logout"><s:message code="header.login.logout" /></a>)
                        </sec:authorize>
                    </div>
                    <div class="dropdown menu blog-nav-item navbar-right locales">
                        <a href="#" class="dropdown-toggle top_level locale ${locale}" data-toggle="dropdown" role="button" aria-label="<s:message code="locale.chooser.current" />">
                            ${locale}
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a aria-label="Русский язык" href="?locale=ru"><span class="locale ru">&nbsp;</span></a></li>
                            <li><a aria-label="English language" href="?locale=en"><span class="locale en">&nbsp;</span></a></li>
                        </ul>
                    </div>                           
                </nav>
            </div>
        </div>