package org.lanseg.travel.db.test;

import java.util.*;

import org.junit.Test;
import org.lanseg.travel.orm.PointMapper;
import org.lanseg.travel.orm.dao.Point;

import static org.junit.Assert.*;
import org.lanseg.travel.orm.dao.PointType;

/**
 *
 * @author lans
 */
public class PointMapperTest extends TestBase {

    private final PointMapper pointMapper;

    public PointMapperTest() {
        pointMapper = session.getMapper(PointMapper.class);
    }

    @Test
    public void testUpdatePoint() {
        Point point = new Point("point_test", testRecord.getId(),
                testTrip.getId(), "Point #", new Date(), Math.random(), Math.random(), null);
        pointMapper.addPoints(Collections.singletonList(point));
        point.setLat(-1);
        point.setLon(-2);
        point.setTitle("Another title");
        point.setTime(new Date());
        pointMapper.updatePoint(point);
        assertEquals(point, pointMapper.getPoint(point.getId()));
        pointMapper.deletePoint(point.getId());
    }

    @Test
    public void testInsertRemove() {
        List<Point> pts = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            pts.add(new Point("pt" + i, testRecord.getId(),
                    testTrip.getId(), "Point #" + i, new Date(),
                    Math.random(), Math.random(), null));
        }
        pointMapper.addPoints(pts);
        List<Point> points = pointMapper.getPoints(testTrip.getId(), true);
        assertEquals("Sizes are equal", pts.size(), points.size());
        assertTrue("Same as in database", pts.containsAll(points));
        points = pointMapper.getPointsForRecord(testRecord.getId());
        assertEquals("[For record] Sizes are equal", pts.size(), points.size());
        assertTrue("[For record] Same as in database", pts.containsAll(points));
        pts.forEach((pt) -> pointMapper.deletePoint(pt.getId()));
        assertTrue("Points removed", pointMapper.getPoints(testTrip.getId(), true).isEmpty());
    }

    @Test
    public void testVisiblePoints() {
        List<Point> pts = new ArrayList<>();
        List<Point> visible = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Point p = new Point("pt" + i, i % 2 == 0 ? testRecord.getId() : publishedRecord.getId(),
                    testTrip.getId(), "Point #" + i, new Date(), Math.random(), Math.random(), null);
            pts.add(p);
            if (i % 2 != 0) {
                visible.add(p);
            }
        }
        pointMapper.addPoints(pts);
        List<Point> invisiblePts = pointMapper.getPoints(testTrip.getId(), true);
        List<Point> visiblePts = pointMapper.getPoints(testTrip.getId(), false);
        List<Point> allPoints = pointMapper.getAllPoints(true);
        pts.forEach((pt -> pointMapper.deletePoint(pt.getId())));
        assertTrue("All points", invisiblePts.containsAll(pts));
        assertTrue("All points", allPoints.containsAll(pts));
        assertTrue("Not visible draft", visible.containsAll(visiblePts));
    }

    @Test
    public void checkAddRemovePointType() {
        Point point = new Point("point_test", testRecord.getId(),
                testTrip.getId(), "Point #", new Date(), Math.random(), Math.random(), null);
        PointType type = new PointType(Integer.MAX_VALUE - 100, "whatever_type_" + Math.random());
        pointMapper.addPoints(Collections.singletonList(point));
        pointMapper.addPointType(type);
        pointMapper.setPointType(point.getId(), type.getId());
        PointType typeFromDB = pointMapper.getPoint(point.getId()).getType();
        boolean sameFromDB = typeFromDB.equals(type);
        pointMapper.deletePointType(type.getId());
        pointMapper.deletePoint(point.getId());
        assertTrue(sameFromDB);
        assertTrue(!pointMapper.getPointTypes().contains(type));
    }
}
