<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container blog">
    <div class="blog-header" style="background-image: url(/files/${trip.id}/bg.jpg); ">
        <sec:authorize access="hasAuthority('editor')">
            <div class="editable">
                <a href="edit"><span class="glyphicon glyphicon-pencil"></span></a>
            </div>      
        </sec:authorize>        
        <div class="blog-title">
            <c:if test="${trip.draft}">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true" title="<s:message code="editor.trip.draft" />"></span>
            </c:if>            
            <h1>${trip.title}</h1>
            <div class="dates">
                <c:choose>
                    <c:when test="${trip.startDate != null}">
                        <fmt:formatDate pattern="dd.MM.yyyy" value="${trip.startDate}"/>
                    </c:when>
                    <c:otherwise>...</c:otherwise>
                </c:choose> &mdash;
                <c:choose>
                    <c:when test="${trip.endDate != null}">
                        <fmt:formatDate pattern="dd.MM.yyyy" value="${trip.endDate}"/>
                    </c:when>
                    <c:otherwise>...</c:otherwise>
                </c:choose>
            </div>            
        </div> 
    </div>
    <div id="map_holder" class="page map" style="z-index: 0;">
        <div id="point_description" class="map_sidebar">
            <div id="hide_pd" class="hide_button">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            </div>
            <div class="blog-post diary">
                <h2 class="caption"><s:message code="map.point_description" /></h2>
                <div class="content"></div>
            </div>
        </div>
        <div id="map" class="map full">
            <img src="/img/crosshair.png" class="crosshair"/> 
        </div>           
        <sec:authorize access="hasAuthority('ROLE_ANONYMOUS')">
            <div id="point_popup" class="point_popup"></div>
        </sec:authorize>        
        <sec:authorize access="hasAuthority('editor')">
            <div id="point_popup" class="point_popup">
                <select class="point_record">
                    <option selected>
                    <s:message code="editor.map.day" />
                    </option>
                    <c:forEach var="record" items="${records}">
                        <option value="${record.id}">${record.title}</option>
                    </c:forEach>
                </select><br/>
                <input type='text' value='<s:message code="editor.map.new_point" />' class='point_name'/><br/>
                <a href='javascript:void(0);' class='delete_point' >
                    <span class='glyphicon glyphicon-remove'></span> <s:message code="editor.map.delete_point" />
                </a>
            </div>            
            <div id="map_editor">
                <div class="editor_controls">
                    <div class="title">Drawing</div>
                    <div class="control group add_point">
                        <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span><s:message code="editor.map.point" />
                    </div>
                    <div class="control group draw_line">
                        <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span><s:message code="editor.map.line" />
                    </div>                    
                    <div class="title">Location</div>                    
                    <div class="control my_location">
                        <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span><s:message code="editor.map.my_location" />
                    </div>                                
                    <div class="control coordinates">
                        <span class="glyphicon glyphicon-hand-down" aria-hidden="true"></span><s:message code="editor.map.coordinates" />
                        <div class="coords">
                            <input type="text" class="lon" />
                            <input type="text" class="lat" />
                            <button type="button">Go</button>
                        </div>
                    </div>
                    <div class="title"><s:message code="editor.map.tracks" /></div>
                    <div class="tracks">
                        <c:forEach var="track" items="${tracks}">
                            <div class="track control" data-track_id="${track.id}">
                                <div class="title">
                                    <a href="#">${track.title}</a>
                                </div>
                            </div>
                        </c:forEach>
                    </div>                    
                </div>
            </div>
        </sec:authorize>
    </div>
    <div class="page diary">            
        <div class="blog-main">
            <div class="blog-description">${trip.description}</div>
            <c:if test="${prevRecord != null || nextRecord != null}">
                <%@include file="/WEB-INF/jspf/navbar.jspf" %>
            </c:if>
            <c:forEach var="record" items="${records}">
                <div class="blog-post">
                    <span id="${record.shortcut}" class="anchor"></span>
                    <sec:authorize access="hasAuthority('editor')">
                        <div class="editable">
                            <a href="/${trip.id}/${record.shortcut}/edit"><span class="glyphicon glyphicon-pencil"></span></a>
                                <c:if test="${record.draft}">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true" title="<s:message code="editor.record.draft" />"></span>
                            </c:if>
                        </div>
                    </sec:authorize>                    
                    <div class="blog-post-title">
                        <a href="/${trip.id}/${record.shortcut}/"><h2>${record.title}</h2></a>
                    </div>
                    <p class="blog-post-meta">${record.description}</p>
                    <div class="body">${record.body}</div>
                    <div class="footer">
                        <div class="sections">
                            <c:if test="${not empty points[record.id]}">
                                <a href="#places" class="button points">
                                    <s:message code="record.places" /> <div class="right">${f:length(points[record.id])}</div>
                                </a>
                            </c:if>
                            <c:if test="${not empty pictures[record.id]}">
                                <a href="#photos" class="button photos">
                                    <s:message code="record.photos" /> <div class="right">${f:length(pictures[record.id])}</div>
                                </a>
                            </c:if>
                            <a href="#comments" class="button comments">
                                <s:message code="record.comments" />
                            </a>
                            <c:if test="${not empty points[record.id]}">
                                <div class="section places">
                                    <c:forEach var="point" items="${points[record.id]}" varStatus="loop">
                                        <a class="poi" href="${point.lon};${point.lat}">${point.title}</a><c:if test="${not loop.last}">,</c:if>
                                    </c:forEach>
                                </div>
                            </c:if>
                            <c:if test="${not empty pictures[record.id]}">
                                <div class="section photos">
                                    <div class="gallery">
                                        <c:forEach var="photo" items="${pictures[record.id]}" varStatus="loop">
                                            <c:choose>
                                                <c:when test="${f:startsWith(photo.path, 'http')}">
                                                    <c:set var="thumb_url" value="${photo.thumbnail}" />
                                                    <c:set var="photo_url" value="${photo.path}" />
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set var="thumb_url" value="/files/${trip.id}/${photo.thumbnail}" />
                                                    <c:set var="photo_url" value="/files/${trip.id}/${photo.path}" />                        
                                                </c:otherwise>
                                            </c:choose>                                          
                                            <a data-lightbox="${record.id}" href="${photo_url}">
                                                <img data-src="${thumb_url}" class="thumb" alt='<c:out value="#${loop.index} ${record.title}${empty photo.title ? \"\" : \":\"}${photo.title}" />' />
                                                <div class="data-title">
                                                    ${loop.index}.&nbsp;${record.title}
                                                    <c:if test="${not empty photo.title}">
                                                        <br/>${photo.title}
                                                    </c:if>
                                                    <c:if test="${(not empty photo.info) and (photo.info.isAuthorShowing())}">
                                                        <br/>© <div class="author" data-userid="${photo.info.authorId}">${photo.info.authorId}</div>
                                                    </c:if>                                                        
                                                </div>
                                            </a>
                                        </c:forEach>
                                    </div>
                                </div>
                            </c:if>                            
                            <div id="vk_comments_${record.id}" class="section comments"></div>
                        </div>
                    </div>
                </div>
            </c:forEach>
            <c:if test="${prevRecord != null || nextRecord != null}">
                <%@include file="/WEB-INF/jspf/navbar.jspf" %>
            </c:if>         
            <sec:authorize access="hasAuthority('editor')">
                <div class="new_record">
                    <a href="add_record"><span class="glyphicon glyphicon-pencil"></span><s:message code="editor.record.add_record" /></a>
                </div>
            </sec:authorize>            
        </div>
        <div class="sidebar">
            <%@include file="/WEB-INF/jspf/sidebar.jspf" %>               
        </div>
    </div>
</div>           
<%@include file="/WEB-INF/jspf/footer.jspf" %>