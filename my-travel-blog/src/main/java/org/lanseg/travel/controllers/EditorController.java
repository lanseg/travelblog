package org.lanseg.travel.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;
import org.lanseg.travel.orm.PictureMapper;
import org.lanseg.travel.orm.PointMapper;
import org.lanseg.travel.orm.RecordMapper;
import org.lanseg.travel.orm.TrackMapper;
import org.lanseg.travel.orm.TripMapper;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.Point;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Track;
import org.lanseg.travel.orm.dao.Trip;
import org.lanseg.travel.utils.storage.Storage;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lans
 */
@Controller
@Secured("editor")
@PreAuthorize("hasAuthority('editor')")
public class EditorController {

    private static final Logger LOG = Logger.getLogger("EditorController");

    @Resource
    private RecordMapper recordMapper;
    @Resource
    private TripMapper tripMapper;
    @Resource
    private PictureMapper pictureMapper;
    @Resource
    private PointMapper pointMapper;
    @Resource
    private TrackMapper trackMapper;
    @Resource
    private Storage storage;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("dd.MM.yyyy"), true));
    }

    @RequestMapping("{tripId}/add_record")
    public String addRecord(ModelMap model, @PathVariable("tripId") String tripId) {
        Record r = new Record(0, tripId, "record_shortcut" + (int) (Math.random() * 10000),
                "New record", "Description", "Body", new Date(), true);
        recordMapper.addRecord(r);
        return "redirect:/" + tripId + "/" + r.getShortcut() + "/edit";
    }

    @RequestMapping("{tripId}/{recordId}/edit")
    public String editRecord(ModelMap model,
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId) {
        Record r = recordMapper.getRecord(tripId, recordId);
        model.addAttribute("tripId", tripId);
        if (r == null) {
            return "not_available";
        }
        model.addAttribute("record", r);
        model.addAttribute("allRecords", recordMapper.getRecordsNoBody(tripId, true));
        model.addAttribute("pictures", pictureMapper.getPictures(r.getId()));
        model.addAttribute("points", pointMapper.getPointsForRecord(r.getId()));
        model.addAttribute("pointTypes", pointMapper.getPointTypes());
        return "record_editor";
    }

    @RequestMapping("{tripId}/{recordId}/save")
    public String saveRecord(ModelMap model,
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @ModelAttribute("record") Record record) {
        Record current = recordMapper.getRecord(tripId, recordId);
        if (current != null) {
            current.setBody(record.getBody());
            current.setTitle(record.getTitle());
            current.setDescription(record.getDescription());
            current.setDate(record.getDate());
            current.setShortcut(record.getShortcut());
            current.setDraft(record.isDraft());
            recordMapper.updateRecord(current);
        }
        return "redirect:/" + tripId + "/#record_"
                + (current == null ? record.getShortcut() : current.getShortcut());
    }

    @RequestMapping("{tripId}/{recordId}/delete")
    public String deleteRecord(ModelMap model,
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId) {
        recordMapper.deleteRecord(recordMapper.getRecord(tripId, recordId).getId());
        return "redirect:/" + tripId + "/";
    }

    @RequestMapping(value = "add_trip", method = RequestMethod.GET)
    public String addTrip(ModelMap model) {
        model.addAttribute("trip", new Trip(null,
                "Full title", "Short title", "Description", new Date(), new Date()));
        return "trip_editor";
    }

    @RequestMapping("{tripId}/edit")
    public String editTrip(ModelMap model,
            @PathVariable("tripId") String tripId) {
        model.addAttribute("trip", tripMapper.getTrip(tripId));
        model.addAttribute("tracks", trackMapper.getTripTracks(tripId));
        model.addAttribute("background_present", storage.fileExists(tripId + "/bg.jpg"));
        return "trip_editor";
    }

    @RequestMapping(value = "submit_trip", method = RequestMethod.POST)
    public String submitTrip(ModelMap model, @ModelAttribute("trip") Trip trip) {
        tripMapper.addTrip(trip);
        storage.initTrip(trip.getId());
        return "redirect:/" + trip.getId() + "/";
    }

    @RequestMapping("{tripId}/save")
    public String saveTrip(ModelMap model,
            @PathVariable("tripId") String tripId,
            @ModelAttribute("trip") Trip trip,
            BindingResult errors) {
        trip.setId(tripId);
        tripMapper.updateTrip(trip);
        return "redirect:/" + tripId + "/";
    }

    @RequestMapping("{tripId}/delete")
    public String deleteTrip(ModelMap model, @PathVariable("tripId") String tripId) {
        tripMapper.deleteTrip(tripId);
        return "redirect:/";
    }

    @RequestMapping(value = "{tripId}/{recordId}/add_picture", method = RequestMethod.POST)
    public String addPicture(ModelMap model,
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @ModelAttribute Picture picture) {
        Record r = recordMapper.getRecord(tripId, recordId);
        picture.setRecordId(r.getId());
        pictureMapper.addPicture(picture);
        return "redirect:/" + tripId + "/" + recordId + "/edit#pictures";
    }

    @RequestMapping(value = "{tripId}/{recordId}/delete_picture")
    public String deletepicture(ModelMap model,
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @RequestParam("id") int id) {
        pictureMapper.deletePicture(id);
        return "redirect:/" + tripId + "/" + recordId + "/edit#pictures";
    }

    @RequestMapping(value = "{tripId}/update_picture")
    public @ResponseBody
    Picture editpictureTitle(@PathVariable("tripId") String tripId,
            @RequestParam("field") String field,
            @RequestParam("value") String value,
            @RequestParam("id") int id) {
        Picture a = pictureMapper.getPicture(id);
        if (field.equals("title")) {
            a.setTitle(value);
        }
        pictureMapper.updatePicture(a);
        return a;
    }

    @RequestMapping(value = "{tripId}/update_point")
    public @ResponseBody
    Point editPoint(@PathVariable("tripId") String tripId,
            @ModelAttribute("point") Point point) {
        pointMapper.updatePoint(point);
        return pointMapper.getPoint(point.getId());
    }

    @RequestMapping(value = "{tripId}/add_point")
    public @ResponseBody
    Point addPointAjax(@PathVariable("tripId") String tripId,
            @RequestParam("lat") double lat,
            @RequestParam("lon") double lon) {
        //TODO: Fix random id generation
        Point newPoint = new Point("new_point_" + (int) (Math.random() * 10000),
                null, tripId, "New Point", null, lon, lat, null);
        pointMapper.addPoints(Collections.singletonList(newPoint));
        return newPoint;
    }

    @RequestMapping(value = "{tripId}/remove_point")
    public @ResponseBody
    String removePointAjax(@PathVariable("tripId") String tripId,
            @RequestParam("id") String pointId) {
        pointMapper.deletePoint(pointId);
        return pointId;
    }

    @RequestMapping(value = "{tripId}/{recordId}/remove_point")
    public String removePoint(@PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @RequestParam("id") String pointId) {
        pointMapper.deletePoint(pointId);
        return "redirect:/" + tripId + "/" + recordId + "/edit";
    }

    @RequestMapping(value = "{tripId}/set_point_type", method = RequestMethod.POST)
    public @ResponseBody
    Point setPointType(@PathVariable("tripId") String tripId,
            @RequestParam("point") String pointId,
            @RequestParam("type") Integer typeId) {
        pointMapper.setPointType(pointId, typeId);
        return pointMapper.getPoint(pointId);
    }

    @RequestMapping("/{tripId}/{recordId}/add_photos")
    public @ResponseBody
    List<Picture> addPhotos(@PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @RequestBody List<Picture> photos) {
        //TODO: Добавление сразу пачки в базу
        Record r = recordMapper.getRecord(tripId, recordId);
        int lastPhoto = pictureMapper.getPictureNumber(r.getId());
        for (Picture p : photos) {
            p.setRecordId(r.getId());
            p.setSortOrder(++lastPhoto);
            pictureMapper.addPicture(p);
            p.getInfo().setPictureId(p.getId());
            pictureMapper.addPictureInfo(p.getInfo());
        }
        return pictureMapper.getPictures(r.getId());
    }

    @RequestMapping("{tripId}/{recordId}/edit_info")
    public @ResponseBody
    boolean editPicture(
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @RequestParam("id") int id,
            @RequestParam("action") String action) {
        Picture p = pictureMapper.getPicture(id);
        if (p.getInfo() == null) {
            return false;
        }
        if (action.equals("show_author_toggle")) {
            p.getInfo().setShowAuthor(!p.getInfo().isAuthorShowing());
            pictureMapper.addPictureInfo(p.getInfo());
        }
        return p.getInfo().isAuthorShowing();
    }

    @RequestMapping("{tripId}/{recordId}/move_photo")
    public @ResponseBody
    boolean swapPictures(
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @RequestParam("source") int sourceId,
            @RequestParam("target") int targetId) {
        Picture source = pictureMapper.getPicture(sourceId);
        Picture target = pictureMapper.getPicture(targetId);
        pictureMapper.updateOrder(source.getRecordId(), source.getId(),
                source.getSortOrder(), target.getSortOrder());
        return false;
    }

    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("{tripId}/delete_track")
    public String deleteTrack(
            @PathVariable("tripId") String tripId,
            @RequestParam("id") int trackId) {
        Track track = trackMapper.getTrack(trackId);
        if (track != null && track.getId() == trackId) {
            trackMapper.deleteTrack(trackId);
            storage.removeForTrip(tripId, track.getFilename());
        }
        return "redirect:/" + tripId + "/edit";
    }

    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("/{tripId}/upload_track")
    public String uploadTrack(@PathVariable("tripId") String tripId,
            @RequestParam("file") MultipartFile file) {
        try {
            MessageDigest hasher = MessageDigest.getInstance("SHA");
            String trackName = DatatypeConverter.printHexBinary(hasher.digest(file.getBytes()))
                    + ".json";
            trackMapper.addTrack(new Track(0, trackName, "New track", tripId, null));
            storage.uploadForTrip(tripId, trackName, file.getInputStream());
        } catch (NoSuchAlgorithmException | IOException ex) {
            LOG.log(Level.SEVERE, "Failed to save track: ", ex.getMessage());
        }
        return "redirect:/" + tripId + "/edit";
    }

    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("/{tripId}/tracks/{trackId}/update")
    public @ResponseBody
    Track updateTrack(@PathVariable("tripId") String tripId,
            @PathVariable("trackId") int id,
            @RequestParam("field") String field,
            @RequestParam("value") String value) {
        Track track = trackMapper.getTrack(id);
        if ("title".equals(field)) {
            track.setTitle(value);
        }
        trackMapper.updateTrack(track);
        return track;
    }

    static class TrackNode {

        private double lon;
        private double lat;
        private double spd;
        private double elv;
        private long time;

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getSpd() {
            return spd;
        }

        public void setSpd(double spd) {
            this.spd = spd;
        }

        public double getElv() {
            return elv;
        }

        public void setElv(double elv) {
            this.elv = elv;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }
    }

    @RequestMapping("/{tripId}/tracks/{trackId}/update_data")
    public @ResponseBody
    Track updateTrackData(@PathVariable("tripId") String tripId,
            @PathVariable("trackId") int id,
            @RequestBody List<TrackNode> points) {
        Track track = trackMapper.getTrack(id);
        ObjectMapper mapper = new ObjectMapper();
        try {
            storage.uploadForTrip(tripId, track.getFilename(),
                    new ByteArrayInputStream(mapper.writeValueAsBytes(points)));
        } catch (JsonProcessingException ex) {
            LOG.log(Level.WARNING, "Can't convert track to json: {0}", ex.getMessage());
        }
        return track;
    }
}
