<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="/WEB-INF/jspf/editor_header.jspf" %>
<h1>${trip.id} (${trip.title})</h1>
<div class="header">
    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
    <a href="delete"><s:message code="editor.trip.delete_trip" /></a>
</div>
<div class="content">
    <div class="row">
        <h3><s:message code="editor.trip.header_picture" />:</h3>
        1200x200px
        <div class="header-image">
            <img id="headerImg" src="/files/${trip.id}/bg.jpg" alt="background image"/>
        </div>
        <div class="uploader">
            <c:if test="${background_present}">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                <a href="delete-background"><s:message code="editor.trip.delete_picture" /></a>
            </c:if>            
            <form action="upload-header?${_csrf.parameterName}=${_csrf.token}" method="post" enctype="multipart/form-data">
                <input class="file" type="file" name="file">
                <button><s:message code="editor.trip.update_picture" /></button>
            </form>
        </div>
    </div>         
</div>
<div class="content">
    <div class="row">
        <h3><s:message code="editor.trip.track" />:</h3>
        <div class="row">
            Массив элементов вида {..., lon: долгота, lat: широта, ...}. Может 
            содержать другие параметры:
            <ul>
                <li><s:message code="editor.trip.elevation" /></li>
                <li><s:message code="editor.trip.speed" /></li>
                <li><s:message code="editor.trip.time" /></li>
            </ul>
        </div>
        <div class="row"><i><s:message code="editor.trip.track_info" /></i></div>
        <div class="row">
            <div class="uploader tracks">
                <c:if test="${!tracks.isEmpty()}">
                    <table class="table" >
                        <thead>
                            <tr>
                                <th><s:message code="editor.trip.track" /></th>
                                <th><s:message code="editor.trip.file" /></th>
                                <th><s:message code="editor.trip.record" /></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="track" items="${tracks}">
                                <tr id="track_${track.id}">
                                    <td class="title"><a href="#">${empty track.title ? "Без имени" : track.title}</a></td>
                                    <td>${track.filename}</td>
                                    <td>${track.tripId}</td>
                                    <td>
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        <a href="delete_track?id=${track.id}"><s:message code="editor.trip.delete_track" /></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:if>
                <form action="upload_track?${_csrf.parameterName}=${_csrf.token}" method="post" enctype="multipart/form-data">
                    <input class="file" type="file" name="file">
                    <button><s:message code="editor.trip.add_track" /></button>
                </form>
            </div>
        </div>
    </div>
</div>            
<div class="content">
    <h3><s:message code="editor.trip.description" /></h3>    
    <c:if test="${empty trip.id}">
        <form method="POST" action="/submit_trip" class="editor">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> 
            <div class="row">
                <input type="text" name="id" value="${trip.id}" />
            </div>
        </c:if>
        <c:if test="${not empty trip.id}">
            <form method="POST" action="/${trip.id}/save" class="editor">
            </c:if>    
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>                 
            <div class="row">
                <input id="draft" type="checkbox" name="draft" <c:if test="${trip.draft}">checked</c:if> />
                    <label for="draft"><s:message code="editor.trip.draft" /></label>
                </div>                
                <div class="row">
                    <input type="text" name="title" value="${trip.title}" />
            </div>
            <div class="row">
                <input type="text" name="shortTitle" value="${trip.shortTitle}" />
            </div>
            <div class="row date">
                <input type="text" name="startDate" value="<f:formatDate pattern="dd.MM.yyyy" value="${trip.startDate}" />" />
                <input type="text" name="endDate" value="<f:formatDate pattern="dd.MM.yyyy" value="${trip.endDate}" />" />
            </div>
            <div class="row">
                <textarea name="description">${trip.description}</textarea>
            </div>
            <div class="row">
                <button><s:message code="editor.trip.save" /></button>
            </div>
        </form>       

</div>
<%@include file="/WEB-INF/jspf/editor_footer.jspf" %>
