package org.lanseg.travel.db.test;

import static org.junit.Assert.*;
import org.junit.Test;
import org.lanseg.travel.orm.dao.Trip;

/**
 *
 * @author lans
 */
public class TripMapperTest extends TestBase {

    @Test
    public void testBasicTripActions() {
        assertEquals(testTrip, tripMapper.getTrip(testTrip.getId()));
    }

    @Test
    public void updateTripTest() {
        testTrip.setDescription("Whatever, anyway");
        tripMapper.updateTrip(testTrip);
        Trip newTrip = tripMapper.getTrip(testTrip.getId());
        assertEquals(testTrip, newTrip);
    }
}
