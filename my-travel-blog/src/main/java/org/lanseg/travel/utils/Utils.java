package org.lanseg.travel.utils;

import java.io.File;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.lanseg.travel.controllers.AuthController;
import org.lanseg.travel.orm.dao.User;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author lans
 */
public class Utils {

    private static final DateTimeFormatter TIME_FORMAT
            = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);

    public static <T, K> Map<K, List<T>> groupByKey(List<T> items, Function<T, K> keyFunc) {
        Map<K, List<T>> result = new HashMap<>();
        items.forEach(t -> {
            K key = keyFunc.apply(t);
            if (!result.containsKey(key)) {
                result.put(key, new ArrayList<>());
            }
            result.get(key).add(t);
        });
        return result;
    }

    public static <T, K> Map<K, T> listToMap(List<T> items, Function<T, K> keyFunc) {
        Map<K, T> result = new HashMap<>();
        items.forEach(t -> {
            K key = keyFunc.apply(t);
            result.put(key, t);
        });
        return result;
    }

    public static String plural(int n, String single, String gen, String plural) {
        if (n == 0 || (n >= 5 && n <= 20)) {
            return plural;
        } else if (n % 10 == 1) {
            return single;
        } else {
            return gen;
        }
    }

    public static String formatFileSize(long size) {
        if (size < 1024) {
            return size + " B";
        }
        int exp = (int) (Math.log(size) / Math.log(size));
        char pre = "KMGTPE".charAt(exp - 1);
        return String.format("%.1f %sB", size / Math.pow(1024, exp), pre);
    }

    public static String formatFileTime(FileTime time) {
        LocalDateTime dt = LocalDateTime.ofInstant(time.toInstant(), ZoneId.of("Europe/Moscow"));
        return dt.format(TIME_FORMAT);
    }


    public static User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
            return (User) auth.getPrincipal();
        } else {
            return AuthController.ANONYMOUS;
        }
    }

    public static boolean isEditor() {
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) 
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return authorities.contains(new SimpleGrantedAuthority("editor"));
    }

    public static String getExtension(String path) {
        return path.substring(path.lastIndexOf(".") + 1, path.length());
    }

    public static String getFilename(String path) {
        return path.substring(path.lastIndexOf(File.pathSeparator) + 1, path.length());
    }
}
