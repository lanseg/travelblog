/* global recordId, appendCSRF, tripId */
/*
 * Gallery editor, support for changing photo order by drag and drop.
 */
(function (root) {
    "use strict";
    root.gui = root.gui || {};

    root.gui.DnD = function (selector) {
        var dragSrcEl = null;
        var cols = document.querySelectorAll(selector);
        [].forEach.call(cols, function (col) {
            col.addEventListener('dragstart', handleDragStart, false);
            col.addEventListener('dragstart', handleDragStart, false);
            col.addEventListener('dragenter', handleDragEnter, false);
            col.addEventListener('dragover', handleDragOver, false);
            col.addEventListener('dragleave', handleDragLeave, false);
            col.addEventListener('drop', handleDrop, false);
            col.addEventListener('dragend', handleDragEnd, false);
        });
        function handleDragStart(e) {
            dragSrcEl = this;
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('text/html', this.innerHTML);

        }
        function handleDragOver(e) {
            this.classList.add('over');
            e.preventDefault();
            e.stopPropagation();
            e.dataTransfer.dropEffect = 'move';
            return false;
        }
        function handleDragEnter(e) {
            this.classList.add('over');
        }
        function handleDragLeave(e) {
            e.preventDefault();
            e.stopPropagation();
            this.classList.remove('over');
        }
        function handleDragEnd(e) {
            [].forEach.call(cols, function (col) {
                col.classList.remove('over');
            });
        }
        function handleDrop(e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            if (dragSrcEl !== this) {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: "/" + tripId + "/" + recordId + "/move_photo",
                    data: {
                        source: dragSrcEl.id.replace("picture_", ""),
                        target: this.id.replace("picture_", "")
                    },
                    beforeSend: appendCSRF,
                    success: function (data) {
                        that.parentNode.insertBefore(dragSrcEl, that);
                    }
                });
            }
            return false;
        }
    };
})(this);