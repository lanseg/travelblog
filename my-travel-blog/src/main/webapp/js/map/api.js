/* global csrfToken, csrfHeader */

(function (root) {
    root.api = {
        maps: {
            addPoint: function (tripId, lon, lat, callback) {
                $.ajax({
                    url: "/" + tripId + "/add_point",
                    data: {lon: lon, lat: lat},
                    success: function (point) {
                        if (callback) {
                            callback(point);
                        }
                    }
                });
            },
            removePoint: function (tripId, pointId, callback) {
                $.ajax({
                    url: "/" + tripId + "/remove_point",
                    data: {id: pointId},
                    success: function (point) {
                        if (callback) {
                            callback(point);
                        }
                    }
                });
            },
            getPoints: function (tripId, callback) {
                var path = tripId ? ("/data/" + tripId + "/points") : "/data/points";
                $.get(path, function (data) {
                    callback(data);
                });
            },
            getTracks: function (tripId, callback) {
                var path = tripId ? ("/data/" + tripId + "/tracks") : "/data/tracks";
                $.get(path, function (response) {
                    response.forEach(function (track) {
                        $.get("/files/" + tripId + "/" + track["filename"],
                                function (response) {
                                    callback([{info: track, points: response}]);
                                });
                    });
                });
            },
            updateTrackData: function (tripId, trackId, points, callback) {
                $.ajax({
                    type: 'POST',
                    url: "/" + tripId + "/tracks/" + trackId + "/update_data",
                    contentType: 'application/json; charset=UTF-8',
                    data: JSON.stringify(points),
                    beforeSend: function (request) {
                        if (csrfToken !== undefined &&
                                csrfHeader !== undefined) {
                            request.setRequestHeader(csrfHeader, csrfToken);
                        }
                    },
                    success: function (actualPoint) {
                        if (callback) {
                            callback(actualPoint);
                        }
                    }
                });
            },
            updatePoint: function (tripId, point, callback) {
                point["type"] = undefined;
                $.ajax({
                    type: "POST",
                    url: "/" + tripId + "/update_point",
                    data: point,
                    beforeSend: function (request) {
                        if (csrfToken !== undefined &&
                                csrfHeader !== undefined) {
                            request.setRequestHeader(csrfHeader, csrfToken);
                        }
                    },
                    success: function (actualPoint) {
                        if (callback) {
                            callback(actualPoint);
                        }
                    }
                });
            }
        }
    };
})(this);