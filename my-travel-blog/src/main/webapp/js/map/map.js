/*jslint browser: true*/
/*jslint white: true */
/*global $, ol, maps, settings, tripId, editor, api */

/*
 * External: settings, maps, ol
 */

(function (root) {
    root.maps = root.maps || {};
    if (editor) {
        return;
    }
    root.maps.CustomMap = function (node, style) {
        var that = this;
        this.styleCache = {};
        this.pointData = {};
        this.style = style;
        this.trackSource = new ol.source.Vector();
        this.pointSource = new ol.source.Vector();
        this.popupHolder = document.getElementById('point_popup');
        this.popup = new ol.Overlay({
            element: this.popupHolder,
            positioning: 'bottom-center',
            stopEvent: true
        });
        this.map = new ol.Map({
            target: node,
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                }),
                new ol.layer.Vector({
                    source: that.trackSource,
                    style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: style.trackStrokeColor,
                            width: style.trackStrokeWidth
                        })
                    })
                }),
                new ol.layer.Vector({
                    source: new ol.source.Cluster({
                        distance: 40,
                        source: that.pointSource
                    }),
                    style: function (feature) {
                        return maps.OsmUtils.getStyle(feature, style, false);
                    }
                })
            ]
        });
        this.map.getView().setMaxZoom(19);
    };

    root.maps.CustomMap.prototype.updatePoints = function (points) {
        var map = this.map;
        var view = map.getView();

        var features = [], i, item;
        for (i = 0; i < points.length; i++) {
            item = points[i];
            this.pointData[item["id"]] = item;
            features.push(maps.OsmUtils.createPointFeature(item));
        }

        this.pointSource.addFeatures(features);
        if (!maps.OsmUtils.updateView(map, settings.mapSettings)) {
            if (features.length !== 0) {
                this.bounds = maps.OsmUtils.getFeatureBounds(features);
                view.fit(this.bounds, map.getSize());
                /*if (points.length === 1) {
                 view.setZoom(17);
                 }*/
            } else {
                view.fit(maps.OsmUtils.wholeMapBounds, map.getSize());
                view.setZoom(2);
            }
        }
        if (features.length !== 0) {
            if (settings.mapSettings["sidePage"] !== null) {
                maps.OsmUtils.updateDescription(settings.mapSettings["sidePage"],
                        this);
                maps.OsmUtils.toggleDescription(true);
                this.map.updateSize();
            }
        }
    };

    root.maps.CustomMap.prototype.updateTracks = function (tracks) {
        var features = [];
        tracks.forEach(function (track) {
            features.push(maps.OsmUtils.createTrackFeature(track));
        });
        this.trackSource.addFeatures(features);
    };

    root.maps.CustomMap.prototype.onZoom = function (zoomValue) {
        $(this.popupHolder).hide();
    };

    root.maps.CustomMap.prototype.onMoveEnd = function (evt) {
        var coords = ol.proj.transform(this.map.getView().getCenter(),
                'EPSG:3857', 'EPSG:4326');
        settings.setCenter(coords[0], coords[1]);
        settings.setZoom(this.map.getView().getZoom());        
    };

    root.maps.CustomMap.prototype.onPointSelected = function (feature) {
        var coord = feature.getGeometry().getCoordinates();
        var content = maps.OsmUtils.getPopup(feature, this.map, this.pointData);
        var popup = this.popupHolder;
        if (content === null) {
            $(popup).hide();
            return;
        }
        this.popup.setPosition(coord);
        $(popup).popover({
            'animation': 'false',
            'placement': 'top',
            'html': true
        });
        popup.innerHTML = "";
        popup.appendChild(content);
        $(popup).show();
    };

    root.maps.CustomMap.prototype.onTrackSelected = function () {

    };

    root.maps.CustomMap.prototype.initMap = function () {
        var map = this.map;
        var view = this.map.getView();
        var popupHolder = this.popupHolder;
        map.addOverlay(this.popup);
        api.maps.getPoints(tripId, this.updatePoints.bind(this));
        api.maps.getTracks(tripId, this.updateTracks.bind(this));
        view.on('change:resolution', this.onZoom.bind(this));
        map.on('moveend', this.onMoveEnd.bind(this));        
        map.on('click', function (evt) {
            var feature = map.forEachFeatureAtPixel(evt.pixel,
                    function (feature) {
                        return feature;
                    });
            if (feature) {
                if (feature.get('type') === 'track') {
                    this.onTrackSelected.bind(this)(feature);
                } else {
                    this.onPointSelected.bind(this)(feature);
                }
            } else {
                $(popupHolder).hide();
            }
        }.bind(this));
    };
})(this);