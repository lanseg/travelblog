package org.lanseg.travel.orm;

import java.util.List;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.PictureInfo;

/**
 *
 * @author lans
 */
public interface PictureMapper {

    void addPicture(Picture picture);
    int getPictureNumber(int recordId);
    List<Picture> getPictures(int recordId);
    List<Picture> getTripPictures(String tripId, boolean isEditor);
    List<Picture> getAllPictures(boolean isEditor);
    Picture getPicture(int id);
    void updatePicture(Picture picture);
    void deletePicture(int id);
    void addPictureInfo(PictureInfo info);
    void deletePictureInfo(int id);
    void updateOrder(int recordId, int id, int oldOrder, int newOrder);
}
