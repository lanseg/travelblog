/*jslint browser: true*/
/*jslint white: true */
/*global $, ol, editor, maps, settings, tripId, api */
(function (root) {
    "use strict";
    root.maps = root.maps || {};
    if (!editor) {
        return;
    }
    root.maps.CustomMap = function (root, style) {
        this.styleCache = {};
        this.style = style;
        this.pointData = {};
        this.trackData = {};
        this.trackSource = new ol.source.Vector();
        this.pointSource = new ol.source.Vector();
        this.popupHolder = document.getElementById('point_popup');
        this.popup = new ol.Overlay({
            element: this.popupHolder,
            positioning: 'bottom-center',
            stopEvent: true
        });
        this.interactions = {};
        this.interactions.select = new ol.interaction.Select({
            style: function (feature) {
                return maps.OsmUtils.getStyle(feature, style, true);
            }
        });
        this.interactions.modify = new ol.interaction.Modify({
            features: this.interactions.select.getFeatures()
        });
        this.interactions.drawPoint = new ol.interaction.Draw({
            features: this.interactions.select.getFeatures(),
            type: "Point"
        });
        this.interactions.drawLine = new ol.interaction.Draw({
            features: this.interactions.select.getFeatures(),
            type: "LineString"
        });
        this.interactions.select.getFeatures().on('add', this.onSelect.bind(this), this);
        this.interactions.select.getFeatures().on('remove', this.onDeselect.bind(this), this);
        this.interactions.modify.on('modifystart', this.onModifyStart.bind(this), this);
        this.interactions.modify.on('modifyend', this.onModifyEnd.bind(this), this);

        this.currentInteraction = this.interactions.select;
        this.map = new ol.Map({
            interactions: ol.interaction.defaults().extend([
                this.interactions.select,
                this.interactions.modify
            ]),
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                }),
                new ol.layer.Vector({
                    source: this.trackSource,
                    style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: this.style.trackStrokeColor,
                            width: this.style.trackStrokeWidth
                        })
                    })
                }),
                new ol.layer.Vector({
                    source: this.pointSource,
                    style: function (feature) {
                        return maps.OsmUtils.getStyle(feature, style, false);
                    }
                })
            ],
            target: root
        });
    };

    root.maps.CustomMap.prototype.onPointAdd = function (pointFeature) {
        var coords = ol.proj.transform(pointFeature.getGeometry().getCoordinates(),
                'EPSG:3857', 'EPSG:4326');
        api.maps.addPoint(tripId, coords[0], coords[1], function (point) {
            pointFeature.set('data', point);
            pointFeature.set('type', 'point');
            this.pointSource.addFeatures([pointFeature]);
        }.bind(this));
        pointFeature.set('data', {"id": "new_point_whatever", "title": "New Point"});
    };

    root.maps.CustomMap.prototype.onLineAdd = function (lineFeature) {
        this.trackSource.addFeatures([lineFeature]);
        console.log("Line added. ");
    };

    root.maps.CustomMap.prototype.onLineChanged = function (lineFeature) {
        console.log("Line changed. ");
    };

    root.maps.CustomMap.prototype.onPointRemove = function (point) {
        var geom = point.getGeometry();
        var coords = ol.proj.transform(geom.getCoordinates(), 'EPSG:3857', 'EPSG:4326');
        var point = point.get('data');
        //Distance is small enough, cartesian distance is ok        
        if (Math.hypot(point.lon - coords[0], point.lat - coords[1]) > 0) {
            point.lon = coords[0];
            point.lat = coords[1];
            api.maps.updatePoint(tripId, point, function (point) {
                var newCoords = ol.proj.transform([point.lon, point.lat], 'EPSG:4326', 'EPSG:3857');
                geom.setCoordinates(newCoords);
            });
        }
    };

    root.maps.CustomMap.prototype.updatePoints = function (points) {

        var features = [], i, item;
        for (i = 0; i < points.length; i++) {
            item = points[i];
            features.push(maps.OsmUtils.createPointFeature(item));
            points[item["id"]] = item;
        }

        var map = this.map;
        var view = map.getView();
        this.pointSource.addFeatures(features);
        if (!maps.OsmUtils.updateView(map, settings.mapSettings)) {
            if (features.length !== 0) {
                this.bounds = maps.OsmUtils.getFeatureBounds(features);
                view.fit(this.bounds, map.getSize());
            } else {
                view.fit(maps.OsmUtils.wholeMapBounds, map.getSize());
                view.setZoom(2);
            }
        }
    };

    root.maps.CustomMap.prototype.updateTracks = function (tracks) {
        var features = [];
        tracks.forEach(function (track) {
            var feature = maps.OsmUtils.createTrackFeature(track);
            this.trackData[track["info"]["id"]] = feature;
            features.push(feature);
        }.bind(this));
        this.trackSource.addFeatures(features);
    };

    root.maps.CustomMap.prototype.onPointUnselected = function (feature) {
        $(this.popupHolder).find('option:selected').prop("selected", false);
        $(this.popupHolder).hide();
        var coords = ol.proj.transform(feature.getGeometry().getCoordinates(),
                'EPSG:3857', 'EPSG:4326');
        var data = feature.get('data');
        data["lon"] = coords[0];
        data["lat"] = coords[1];
        api.maps.updatePoint(tripId, data, this.onPointUpdated.bind(this));
    };

    root.maps.CustomMap.prototype.onPointSelected = function (feature) {
        var popup = this.popup;
        var popupHolder = this.popupHolder;
        var geometry = feature.getGeometry();
        var coord = geometry.getCoordinates();
        var data = feature.get("data");
        if (data !== undefined) {
            popup.setPosition(coord);
            $(popupHolder).popover({
                'animation': 'false',
                'placement': 'top',
                'html': true
            });
            $(popupHolder).find('.point_name').val(data["title"]);
            if (data["recordId"] !== null) {
                $(popupHolder).find('option[value="' + data["recordId"] + '"]').prop("selected", true);
            }
            $(popupHolder).show();
        }
    };

    root.maps.CustomMap.prototype.onTrackUnselected = function (arg) {
        var trackSelectors = document.querySelectorAll(".editor_controls .tracks .track");
        for (var i = 0; i < trackSelectors.length; i++) {
            var sel = trackSelectors[i];
            sel.className = sel.className.replace(' down', '');
        }
    };

    root.maps.CustomMap.prototype.onTrackSelected = function (arg) {
        var trackSelectors = document.querySelectorAll(".editor_controls .tracks .track");
        var trackId = arg === undefined ? "" : arg.get('data')["id"];
        for (var i = 0; i < trackSelectors.length; i++) {
            var sel = trackSelectors[i];
            if (sel.dataset.track_id === trackId.toString()) {
                sel.className += ' down';
            } else {
                sel.className = sel.className.replace(' down', '');
            }
        }
    };

    root.maps.CustomMap.prototype.onZoom = function (zoomValue) {
        settings.setZoom(this.map.getView().getZoom());
        $(this.popupHolder).hide();
    };

    root.maps.CustomMap.prototype.onMoveEnd = function () {
        var coords = ol.proj.transform(this.map.getView().getCenter(),
                'EPSG:3857', 'EPSG:4326');
        settings.setCenter(coords[0], coords[1]);
    };

    root.maps.CustomMap.prototype.onPointUpdated = function (point, remove) {
        var f = this.pointSource.getFeatures();
        var toBeUpdated = null;
        for (var i = 0; i < f.length; i++) {
            if (f[i].get('data')["id"] === point.id) {
                toBeUpdated = f[i];
                break;
            }
        }
        if (remove) {
            this.pointSource.removeFeature(toBeUpdated);
            this.interactions.select.getFeatures().clear();
        } else {
            toBeUpdated.set("data", point);
            maps.OsmUtils.clearStyle(point["id"]);
            toBeUpdated.changed();
        }
        $("#point_popup").hide();
    };

    root.maps.CustomMap.prototype.deletePoint = function () {
        var feature = this.interactions.select.getFeatures().item(0);
        if (!feature) {
            return;
        }
        api.maps.removePoint(tripId, feature.get("data")["id"], function (id) {
            this.onPointUpdated({id: id}, true);
        }.bind(this));
    };

    root.maps.CustomMap.prototype.renamePoint = function (newName) {
        var feature = this.interactions.select.getFeatures().item(0);
        if (!feature) {
            return;
        }
        var data = feature.get("data");
        data.title = newName;
        api.maps.updatePoint(tripId, data, this.onPointUpdated.bind(this));
    };

    root.maps.CustomMap.prototype.setPointRecord = function (newRecord) {
        var feature = this.interactions.select.getFeatures().item(0);
        if (!feature) {
            return;
        }
        var data = feature.get("data");
        data.recordId = newRecord;
        api.maps.updatePoint(tripId, data, this.onPointUpdated.bind(this));
    };

    root.maps.CustomMap.prototype.deletePoint = function () {
        var feature = this.interactions.select.getFeatures().item(0);
        if (!feature) {
            return;
        }
        api.maps.removePoint(tripId, feature.get("data")["id"], function (id) {
            this.onPointUpdated({id: id}, true);
        }.bind(this));
    };

    root.maps.CustomMap.prototype.removePoint = function (newName) {
        var feature = this.interactions.select.getFeatures().item(0);
        if (!feature) {
            return;
        }
        var data = feature.get("data");
        data.title = newName;
        api.maps.updatePoint(tripId, data, this.onPointUpdated);
    };

    root.maps.CustomMap.prototype.setEditor = function (editorId) {
        var editor = $(document.getElementById(editorId)),
                popup = $("#point_popup"),
                that = this,
                control;
        editor.find(".control.group").on('click', function () {
            if (control) {
                control.classList.remove('down');
            }
            if (control === this) {
                control = null;
            } else {
                this.classList.add('down');
                control = this;
            }
        });

        this.toggleInteraction = function (act) {
            this.map.removeInteraction(this.currentInteraction);
            if (this.currentInteraction === act) {
                act = this.interactions.select;
            }
            this.map.addInteraction(act);
            this.currentInteraction = act;
        };

        editor.find(".add_point").on('click', function () {
            this.toggleInteraction(this.interactions.drawPoint);
        }.bind(this));

        editor.find('.control.draw_line').on('click', function (e) {
            this.toggleInteraction(this.interactions.drawLine);
        }.bind(this));

        editor.find('.my_location').on('click', function () {
            maps.OsmUtils.getMyLocation(function (pos) {
                var center = new ol.geom.Point(ol.proj.transform([pos.lon, pos.lat], 'EPSG:4326', 'EPSG:3857'));
                this.map.getView().setCenter(center.getCoordinates());
                this.map.getView().setZoom(16);
            });
        });
        editor.find('.coordinates').on('click', function () {
            $(this).find('.coords').toggle();
        });
        editor.find('.editor_controls .coords').on('click', function (e) {
            e.stopPropagation();
        });
        editor.find('.editor_controls .coords button').on('click', function (e) {
            //TODO: Move that to "goto" method of this;
            var parent = $($(this).parent()),
                    lon = parseFloat(parent.children().eq(0).val()),
                    lat = parseFloat(parent.children().eq(1).val()),
                    center = new ol.geom.Point(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
            this.map.getView().setCenter(center.getCoordinates());
            this.map.getView().setZoom(12);
        });
        popup.find(".delete_point").on('click', this.deletePoint.bind(this));
        popup.find('.point_name').on("keyup", function (e) {
            var code = e.keyCode;
            if (code === 27 || code === 13) {
                if (code === 13) {
                    that.renamePoint(this.value);
                    popup.hide();
                }
            }
        });
        popup.find('.point_record').on("change", function (e) {
            that.setPointRecord(this.value);
        });
        this.interactions.drawPoint.on('drawend', function (event) {
            this.onPointAdd(event.feature);
        }.bind(this));
        this.interactions.drawLine.on('drawend', function (event) {
            event.feature.set('type', 'track');
            this.onLineAdd(event.feature);
        }.bind(this));
        var trackButtons = document.querySelectorAll(".editor_controls .tracks .track");
        for (var i = 0; i < trackButtons.length; i++) {
            var node = trackButtons[i];
            var map = this;
            $(node).on('click', function () {
                var track = this.dataset.track_id;
                var features = map.interactions.select.getFeatures();
                features.clear();
                features.push(map.trackData[track]);
            });
        }
        ;
    };

    root.maps.CustomMap.prototype.onSelect = function (event) {
        var feature = event.element;
        if (feature.get('type') === 'track') {
            this.onTrackSelected.bind(this)(feature);
        } else if (feature.get('type') === 'point') {
            console.log(this);
            this.onPointSelected.bind(this)(feature);
        }
    };

    root.maps.CustomMap.prototype.onDeselect = function (event) {
        var feature = event.element;
        if (feature.get('type') === 'track') {
            this.onTrackUnselected.bind(this)(feature);
        } else if (feature.get('type') === 'point') {
            this.onPointUnselected.bind(this)(feature);
        }
        $(this.popupHolder).hide();
    };

    root.maps.CustomMap.prototype.onModifyStart = function (event) {

    };

    root.maps.CustomMap.prototype.onModifyEnd = function (event) {
        event.features.forEach(function (feature) {
            if (feature.get('type') === 'track') {
                var newTrack = [];
                feature.getGeometry().getCoordinates().forEach(function (e) {
                    var lonlat = ol.proj.transform([e[0], e[1]], 'EPSG:3857', 'EPSG:4326');
                    newTrack.push({
                        lon: lonlat[0],
                        lat: lonlat[1],
                        time: e[2],
                        spd: e[3],
                        elv: e[4]
                    });
                });
                api.maps.updateTrackData(tripId, feature.get('data')["id"],
                        newTrack, function (data) {

                        });
            }
        });
    };

    root.maps.CustomMap.prototype.initMap = function () {
        var map = this.map;
        var view = this.map.getView();
        map.addOverlay(this.popup);
        api.maps.getPoints(tripId, this.updatePoints.bind(this));
        api.maps.getTracks(tripId, this.updateTracks.bind(this));
        map.on('moveend', this.onMoveEnd.bind(this));
        view.on('change:resolution', this.onZoom.bind(this));
        this.setEditor('map_editor');
    };
})(this);