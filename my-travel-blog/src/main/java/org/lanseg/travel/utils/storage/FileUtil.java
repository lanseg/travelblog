package org.lanseg.travel.utils.storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.utils.ImageTransformUtil;

/**
 *
 * @author lans
 */
public class FileUtil {

    private static final int PIPE_BUFFER_SIZE = 8192;
    private final String root;

    public FileUtil(String path) {
        this.root = path.replace("file:", "");
    }

    public String getPath() {
        return root;
    }

    private void uploadFile(Path destination, InputStream in) throws IOException {
        try (OutputStream out = Files.newOutputStream(destination, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING)) {
            int readSize;
            byte[] pipeBuffer = new byte[PIPE_BUFFER_SIZE];
            while ((readSize = in.read(pipeBuffer)) != -1) {
                out.write(pipeBuffer, 0, readSize);
            }
        }
    }
    
    private void removeFile(Path destination) throws IOException {
        Files.deleteIfExists(destination);
    }

    public String uploadForTrip(String tripId, String name, InputStream in) throws IOException {
        Path destination = Paths.get(root, tripId, name);
        uploadFile(destination, in);
        return destination.toString();
    }
    
    public void removeForTrip(String tripId, String name) throws IOException {
        removeFile(Paths.get(root, tripId, name));
    }
    
    public String upload(String tripId, String recordId, String name, InputStream in) throws IOException {
        Path destination = Paths.get(root, tripId, "photos", recordId, name);
        if (!Files.exists(destination, LinkOption.NOFOLLOW_LINKS)) {
            Files.createDirectories(destination.getParent());
        }
        uploadFile(destination, in);
        return Paths.get("/", "photos", recordId, name).toString();
    }

    public void rotatePicture(String tripId, Picture picture) throws IOException {
        String img = Paths.get(root, tripId, picture.getPath()).toString();
        String thumb = Paths.get(root, tripId, picture.getThumbnail()).toString();
        ImageTransformUtil.turn(img);
        ImageTransformUtil.turn(thumb);
    }

    public String getRelativePath(String absolute) {
        return absolute.replace(root, "");
    }

    public String getAbsolutePath(String relative) {
        return Paths.get(root, relative).toString();
    }
    
    public boolean fileExists(String path) {
        return Files.exists(Paths.get(root, path), LinkOption.NOFOLLOW_LINKS);
    }
}
