<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">                     
        <title>${trip.shortTitle}</title>
        <link rel="stylesheet" href="/css/gallery.css" />        
        <script src="https://yastatic.net/jquery/2.2.0/jquery.min.js"></script>            
    </head>
    <body>
        <div class="header">
            <div class="back"><h1><a href="/${trip.id}" title="<s:message code="gallery.to_diary" />">&#8592;</a></h1></div>
            <h1>
                ${trip.title}
                <c:if test="${trip.title != null}">: все</c:if>
                <c:if test="${trip.title == null}">Все</c:if>
                фотки
            </h1>
        </div>
        <div class="photo_holder overview">
            <a href="#" class="photo_only_toggle">⤢</a>
            <div class="overlay">
                <div class="controls">
                    <div class="prev">
                        <div class="arrow">◀</div>
                    </div>
                    <div class="next">
                        <div class="arrow">▶</div>
                    </div>
                    <img id="view" src="" />
                </div>
                <div class="description"></div>
            </div>
        </div>            
        <div class="gallery_holder overview">
            <div class="gallery">
                <c:forEach var="photo" items="${pictures}" varStatus="loop">
                    <c:choose>
                        <c:when test="${f:startsWith(photo.path, 'http')}">
                            <c:set var="thumb_url" value="${photo.thumbnail}" />
                            <c:set var="photo_url" value="${photo.path}" />
                        </c:when>
                        <c:otherwise>
                            <c:set var="thumb_url" value="/files/${trip.id}/${photo.thumbnail}" />
                            <c:set var="photo_url" value="/files/${trip.id}/${photo.path}" />                        
                        </c:otherwise>
                    </c:choose>                                          
                    <div class="photo" data-original="${photo_url}">
                        <div class="thumb_holder">
                            <img src="${thumb_url}" class="thumb" alt='<c:out value="#${loop.index} ${record.title}${empty photo.title ? \"\" : \":\"}${photo.title}" />' />
                        </div>                                
                        <div class="data-title">
                            <c:if test="${not empty photo.title}">${photo.title}</c:if>
                            <c:if test="${(not empty photo.info) and (photo.info.isAuthorShowing())}">
                                © <div class="author" data-userid="${photo.info.authorId}"></div>
                            </c:if>                                                        
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <script src="/js/gui/photos.js" ></script>
        <script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>        
        <script src="/js/social/vk_utils.js" ></script>
        <script>
            social.vk.init(true);
            social.vk.updateAuthors();
        </script>        
    </body>
</html>