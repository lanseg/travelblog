package org.lanseg.travel.controllers;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import org.lanseg.travel.orm.dao.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author lans
 */
@Controller
public class AuthController {

    private static final Logger LOG = Logger.getLogger("AUTH");
    public static final User ANONYMOUS = new User("anonymous", "anonymous", "anonymous");

    @Resource
    private AuthenticationManager authenticationManager;

    @RequestMapping("/login")
    public String login(ModelMap model,
            @RequestParam("username") String username,
            @RequestParam("password") String password) {
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authResult = authenticationManager.authenticate(authRequest);
        SecurityContextHolder.getContext().setAuthentication(authResult);
        return "redirect:/";
    }

    @RequestMapping("/logout")
    public String logout() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User) {
            User user = (User) principal;
            LOG.log(Level.INFO, "User {0} logged out.", user.getUsername());
        }
        SecurityContextHolder.clearContext();
        return "redirect:/";
    }
}
