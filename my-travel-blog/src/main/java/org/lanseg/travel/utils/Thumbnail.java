package org.lanseg.travel.utils;

/**
 *
 * @author lans
 */
public class Thumbnail {

    private int width;
    private int height;
    private String thumbnailPath;
    private String originalPath;
    
    public Thumbnail() {
        
    }
    
    public Thumbnail(String path, String originalPath, int width, int height) {
        this.thumbnailPath = path;
        this.originalPath = originalPath;
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String path) {
        this.thumbnailPath = path;
    }

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }
    
    @Override
    public String toString() {
        return String.format("Thumbnail {src: %s, thumb: %s, size:%dx%d}",
                originalPath, thumbnailPath, width, height);
    }
}
