package org.lanseg.travel.text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author lans
 */
public class Tag {

    public static final Set<String> REQUIRES_CLOSING = new HashSet<>(Arrays.asList(
            "point", "comment"
    ));

    private static final Pattern SEPARATOR = Pattern.compile("([^\"]\\S*|\".+?\")\\s*");
    private final String name;
    private final List<String> arguments;
    private final boolean isClosing;
    private final boolean isSelfClosing;

    public Tag(String tag) {
        List<String> tokens = new ArrayList<>();
        Matcher m = SEPARATOR.matcher(tag.substring(1, tag.length() - 1));
        while (m.find()) {
            String token = m.group(1);
            if (token.startsWith("\"") && token.endsWith("\"")) {
                token = token.substring(1, token.length() - 1);
            }
            tokens.add(token);
        }
        String toBeName = tokens.get(0);
        isClosing = toBeName.startsWith("/");
        this.name = isClosing ? toBeName.substring(1) : toBeName;
        this.arguments = tokens.subList(1, tokens.size());
        this.isSelfClosing = !REQUIRES_CLOSING.contains(this.name);
    }

    public String getName() {
        return name;
    }

    public List<String> getArguments() {
        return Collections.unmodifiableList(arguments);
    }

    public boolean isSelfClosing() {
        return isSelfClosing;
    }

    public boolean isClosing() {
        return isClosing;
    }

    public boolean isClosingOf(Tag t) {
        return name != null && t != null && isClosing && name.equals(t.name);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Tag)) {
            return false;
        }
        Tag tag = (Tag) o;
        return tag.isClosing == isClosing && tag.name.equals(name)
                && tag.getArguments().equals(arguments);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.arguments);
        hash = 83 * hash + (this.isClosing ? 1 : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Tag {%s: %s, closing: %b, selfClosing: %b}",
                name, arguments, isClosing, isSelfClosing);
    }
}
