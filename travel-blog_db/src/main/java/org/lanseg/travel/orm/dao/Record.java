package org.lanseg.travel.orm.dao;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author lans
 */
public class Record {

    private int id;
    private String tripId;
    private String title;
    private String body;
    private String description;
    private String shortcut;
    private boolean draft;
    private Date date;

    public Record() {
    }

    public Record(int id, String tripId, String shortcut, String title, String description,
            String body, Date date, boolean draft) {
        this.id = id;
        this.tripId = tripId;
        this.title = title;
        this.body = body;
        this.description = description;
        this.date = date;
        this.shortcut = shortcut;
        this.draft = draft;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }
    
    public String getShortcut() {
        return shortcut;
    }
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Record)) {
            return false;
        }
        Record r1 = (Record) o;
        return r1.id == id && r1.tripId.equals(tripId) && r1.description.equals(description)
                && r1.title.equals(title) && r1.body.equals(body) && r1.shortcut.equals(shortcut) &&
                r1.draft == draft;
    }

    @Override
    public int hashCode() {
        int hash = id;
        hash = 47 * hash + Objects.hashCode(this.tripId);
        hash = 47 * hash + Objects.hashCode(this.title);
        hash = 47 * hash + Objects.hashCode(this.body);
        hash = 47 * hash + Objects.hashCode(this.description);
        hash = 47 * hash + Objects.hashCode(this.shortcut);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Record {id:%d, tripId:%s, shortcut: %s, title: %s, body size: %d}",
                id,
                tripId == null ? "null" : tripId,
                shortcut == null ? "null" : shortcut,
                title == null ? "null" : title,
                body == null ? 0 : body.length());
    }
}
