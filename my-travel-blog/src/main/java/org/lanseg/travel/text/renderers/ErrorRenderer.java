package org.lanseg.travel.text.renderers;

import org.lanseg.travel.text.Tag;
import org.lanseg.travel.text.TagRenderer;

/**
 *
 * @author lans
 */
public class ErrorRenderer implements TagRenderer {

    @Override
    public String render(Tag tag) {
        return String.format("<span class='tag_error'>%s%s</span>",
                tag.isClosing() ? "/" : "", tag.getName());
    }

}
