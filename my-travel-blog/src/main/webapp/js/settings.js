(function (root) {
    "use strict";
    var settings = {
        currentPage: "",
        scroll: 0,
        mapSettings: {
            lon: null,
            lat: null,
            zoom: 1,
            sidePage: null
        },
        setCenter: function (lon, lat) {
            this.mapSettings["lon"] = lon;
            this.mapSettings["lat"] = lat;
            this.updateHref();
        },
        setZoom: function (zoom) {
            this.mapSettings["zoom"] = zoom;
            this.updateHref();
        },
        setSidePage: function (sidePage) {
            this.mapSettings["sidePage"] = sidePage;
            this.updateHref();
        },
        setCurrentPage: function (page) {
            this.currentPage = page;
            this.updateHref();
        },
        update: function (settings) {
            if (settings.hasOwnProperty("currentPage")) {
                this.currentPage = settings["currentPage"];
            }
            if (settings.hasOwnProperty("zoom")) {
                this.mapSettings["zoom"] = parseInt(settings["zoom"]);
            }
            if (settings.hasOwnProperty("lon")) {
                this.mapSettings["lon"] = parseFloat(settings["lon"]);
            }
            if (settings.hasOwnProperty("lat")) {
                this.mapSettings["lat"] = parseFloat(settings["lat"]);
            }
            if (settings.hasOwnProperty("sidePage")) {
                this.mapSettings["sidePage"] = settings["sidePage"];
            }
        },
        getFromHref: function () {
            var params = window.location.hash.split("&"),
                    result = {}, i = 0, param = [];
            if (params[0].startsWith("#")) {
                result["currentPage"] = params[0].substring(1);
                i = 1;
            }
            for (; i < params.length; i++) {
                param = params[i].split("=");
                result[param[0]] = param[1];
            }
            this.update(result);
        },
        updateHref: function () {
            var s = this, hash = "#" + s.currentPage;
            switch (s.currentPage) {
                case "map":
                    if (s.mapSettings.hasOwnProperty("zoom")) {
                        hash += "&zoom=" + s.mapSettings["zoom"];
                    }
                    if (s.mapSettings.hasOwnProperty("lon") &&
                            s.mapSettings.hasOwnProperty("lat") &&
                            s.mapSettings["lon"] !== null &&
                            s.mapSettings["lat"] !== null) {
                        hash += "&lon=" + s.mapSettings["lon"].toFixed(6) +
                                "&lat=" + s.mapSettings["lat"].toFixed(6);
                    }
                    if (s.mapSettings.hasOwnProperty("sidePage") &&
                            s.mapSettings["sidePage"] !== null) {
                        hash += "&sidePage=" + s.mapSettings["sidePage"];
                    }
                    break;
            }
            window.location.hash = hash;
        }
    };
    settings.getFromHref();
    root.settings = settings;
})(this);