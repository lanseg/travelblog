window.onload = function () {
    "strict";
    var img = $(document.getElementById("view"));
    var imgHost = img.parent().parent();
    var current = null;
    var isLoading = false;
    var overview = true;
    var photoOnly = false;

    function toggleFullScreen(state) {
        if (state) {  // current working methods
            var node = document.documentElement;
            if (node.requestFullscreen) {
                node.requestFullscreen();
            } else if (node.mozRequestFullScreen) {
                node.mozRequestFullScreen();
            } else if (node.webkitRequestFullscreen) {
                node.webkitRequestFullscreen();
            }
        } else {
            if (document.cancelFullScreen) {
                document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
                document.webkitCancelFullScreen();
            }
        }
    }


    function setPhoto(photo, centerTo) {
        isLoading = true;
        if (current !== null) {
            current.removeClass('current');
        }
        current = $(photo);
        current.addClass('current');
        imgHost.addClass('loading');
        imgHost.children(".description").html(current.children(".data-title")
                .html().trim());
        img.fadeOut({
            duration: 'fast',
            complete: function (e) {
                img.attr('src', photo.getAttribute('data-original'));
            }
        });
        var holder = $(".gallery_holder").get(0);
        if (centerTo) {
            holder.scrollLeft = photo.offsetLeft + photo.offsetWidth / 2 -
                    holder.offsetWidth / 2;
        }
    }
    function nextPhoto() {
        var next = current.next();
        if (next.size() > 0) {
            setPhoto(next.get(0));
        }
    }
    function prevPhoto() {
        var prev = current.prev();
        if (prev.size() > 0) {
            setPhoto(prev.get(0));
        }
    }
    document.addEventListener('keydown', function (e) {
        if (!isLoading) {
            if (e.keyCode === 37) {
                prevPhoto();
            } else if (e.keyCode === 39 || e.keyCode === 32) {
                nextPhoto();
            }
        }
    });
    var photos = $(".gallery .photo");
    if (photos.size() > 0) {
        setPhoto($(".gallery .photo").first().get(0));
    }
    img.on('load', function (evt) {
        img.fadeIn('fast', function (e) {
            imgHost.removeClass('loading');
            isLoading = false;
        });
    });
    $(".next").on('click', nextPhoto);
    $(".prev").on('click', prevPhoto);
    $(".gallery .photo").on('click', function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        if (overview) {
            img.toggle();
            $(".gallery_holder").removeClass('overview');
            $(".photo_holder").removeClass('overview');
        }
        setPhoto(this, overview);
        overview = false;
    });
    $(".photo_only_toggle").on('click', function (evt) {
        if (photoOnly) {
            $(".gallery_holder").removeClass('photo_only');
            $(".photo_holder").removeClass('photo_only');
            $(".header").removeClass('photo_only');
        } else {
            $(".gallery_holder").addClass('photo_only');
            $(".photo_holder").addClass('photo_only');
            $(".header").addClass('photo_only');
        }
        photoOnly = !photoOnly;
        toggleFullScreen(photoOnly);        
    });
};