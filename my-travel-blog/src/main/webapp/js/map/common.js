/*jslint browser: true*/
/*jslint white: true */
/*global $, ol, maps, csrfToken, csrfHeader, gui, settings */
(function (root) {
    "use strict";
    root.maps = root.maps || {};
    root.maps.OsmUtils = {
        styleCache: {},
        wholeMapBounds: new ol.geom.Polygon([
            [[-10, 10], [10, 10], [10, -10], [-10, -10]]
        ]),
        createPointFeature: function (point) {
            var coord = ol.proj.transform([point.lon, point.lat], 'EPSG:4326',
                    'EPSG:3857');
            return new ol.Feature({
                type: 'point',
                geometry: new ol.geom.Point(coord),
                data: point
            });
        },
        createTrackFeature: function (track) {
            var path = [], points = track["points"];
            for (var i = 0; i < points.length; i++) {
                var point = ol.proj.transform([points[i].lon, points[i].lat],
                        'EPSG:4326', 'EPSG:3857');
                point[2] = points[i].time;
                point[3] = points[i].spd;
                point[4] = points[i].elv;
                path.push(point);
            }
            return new ol.Feature({
                type: 'track',
                data: track["info"],
                geometry: new ol.geom.LineString(path)
            });
        },
        getClusterId: function (features) {
            var result = [], i = 0;
            for (i = 0; i < features.length; i++) {
                result.push(features[i].get('data').id);
            }
            return result.sort().join('');
        },
        getTitle: function (feature) {
            return feature.get('data').title;
        },
        getLabelText: function (features) {
            var text,
                    size = features.length,
                    getTitle = maps.OsmUtils.getTitle;
            if (size > 2) {
                text = getTitle(features[0]) + "\n...\n" + getTitle(features[size - 1]);
            } else if (size === 2) {
                text = getTitle(features[0]) + ",\n" + getTitle(features[1]);
            } else {
                text = getTitle(features[0]);
            }
            return text;
        },
        createLabel: function (text, style, offset, outline, weight) {
            var textStyle = {
                text: text.toString(),
                offsetY: offset,
                textAlign: 'center',
                textBaseline: "middle",
                font: style.fontSize + "px " + style.fontName,
                fill: new ol.style.Fill({color: style.fontColor})
            };
            if (weight !== undefined) {
                textStyle.font = weight + " " + textStyle.font;
            }
            if (outline !== undefined && outline) {
                textStyle.stroke = new ol.style.Stroke({
                    color: style.fontStrokeColor,
                    width: style.fontStrokeWidth
                });
            }
            return new ol.style.Style({text: new ol.style.Text(textStyle)});
        },
        createMultilineLabel: function (text, style, interval, offset) {
            var lines = text.split("\n"), nodes = [], i;
            for (i = 0; i < lines.length; i++) {
                nodes.push(maps.OsmUtils.createLabel(lines[i], style,
                        offset + (style.fontSize + interval) * i, true));
            }
            return nodes;
        },
        getStyle: function (feature, mapStyle, isSelected) {
            switch (feature.get('type')) {
                case 'track':
                    return maps.OsmUtils.getTrackStyle(feature, mapStyle, isSelected);
                default:
                    return maps.OsmUtils.getPointStyle(feature, mapStyle, isSelected);
            }
        },
        getTrackStyle: function (feature, mapStyle, isSelected) {
            return [new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: mapStyle.trackSelectedStrokeColor,
                        width: mapStyle.trackSelectedStrokeWidth
                    })
                })];
        },
        createSingleStyle: function (type) {
            return new ol.style.Icon({
                anchor: [0.5, 37],
                offset: maps.style.icons[type],
                size: [32, 37],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: 0.95,
                src: '/img/icons.png'
            });
        },
        createClusterStyle: function (mapStyle, radius, isSelected) {
            return new ol.style.Circle({
                radius: radius,
                fill: new ol.style.Fill({
                    color: (isSelected ? mapStyle.pointSelectedColor : mapStyle.pointColor)
                })
            });
        },
        clearStyle: function (id) {
            delete maps.OsmUtils.styleCache[id];
            delete maps.OsmUtils.styleCache["*" + id];
        },
        getPointStyle: function (feature, mapStyle, isSelected) {
            if (feature.get('features') === undefined && feature.get('data') === undefined) {
                return;
            }
            var current = feature.get('features') || [feature],
                    id = (isSelected ? '*' : '') + maps.OsmUtils.getClusterId(current),
                    text = maps.OsmUtils.getLabelText(current),
                    style = maps.OsmUtils.styleCache[id],
                    data = current[0].get('data'),
                    radius = mapStyle.pointRadius * Math.min(2.5, current.length),
                    offset = radius;
            if (document.getElementById(data.id + "_description") !== null &&
                    current.length < 2) {
                mapStyle.fontStrokeColor = '#cdefff';
            } else {
                mapStyle.fontStrokeColor = 'white';
            }
            if (!style) {
                style = [new ol.style.Style({
                        image: (current.length > 1 || data.type === null) ?
                                maps.OsmUtils.createClusterStyle(mapStyle, radius, isSelected) :
                                maps.OsmUtils.createSingleStyle(data.type.type)
                    })];
                if (current.length > 1) {
                    style = style.concat(maps.OsmUtils.createLabel(current.length, mapStyle, 0, false, "bold"));
                }
                if (current.length > 1 || data.type === null) {
                    offset = radius + 10;
                }
                style = style.concat(maps.OsmUtils.createMultilineLabel(text, mapStyle, 2, offset));
                maps.OsmUtils.styleCache[id] = style;
            }
            return style;
        },
        getMyLocation: function (callback) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (pos) {
                    callback({
                        lat: pos.coords.latitude,
                        lon: pos.coords.longitude});
                });
            }
        },
        setCenter: function (map, coords) {
            var coord = ol.proj.transform([coords.lon, coords.lat],
                    'EPSG:4326', 'EPSG:3857');
            map.getView().setCenter(coord);
        },
        zoomTo: function (map, id) {
            var coords = id.split(';');
            var zoom = 18;
            var coord = {lon: parseFloat(coords[0]), lat: parseFloat(coords[1])};
            maps.OsmUtils.setCenter(map, coord);
            map.getView().setZoom(zoom);
        },
        getFeatureBounds: function (features) {
            var c = [[Number.MAX_VALUE, Number.MAX_VALUE],
                [Number.MIN_VALUE, Number.MIN_VALUE]];
            for (var i = 0; i < features.length; i++) {
                var cc = features[i].getGeometry().getCoordinates();
                c[0][0] = Math.min(c[0][0], cc[0]);
                c[0][1] = Math.min(c[0][1], cc[1]);
                c[1][0] = Math.max(c[1][0], cc[0]);
                c[1][1] = Math.max(c[1][1], cc[1]);
            }
            return new ol.geom.Polygon([
                [c[0], [c[1][0], c[0][1]], c[1], [c[0][0], c[1][1]]]
            ]);
        },
        updateDescription: function (id, map, pointData) {
            var block = document.getElementById(id + "_description"),
                    data = pointData[id];
            if (block !== null) {
                var description = $("#point_description");
                description.find(".caption").html(data["title"]);
                description.find(".content").html(block.innerHTML);
                gui.gallery.init("#point_description .img-box .content");
                settings.setSidePage(data.id);
                return true;
            }
            return false;
        },
        toggleDescription: function (visible) {
            if (visible) {
                $("#map").removeClass('full');
                $("#map").addClass('split');
                $("#point_description").show();
            } else {
                $("#point_description").hide();
                $("#map").addClass('full');
                $("#map").removeClass('split');
                settings.setSidePage(null);
            }
        },
        getPopup: function (feature, map, pointData) {
            var features = feature.get('features'),
                    l = features.length, id, links;
            if (l > 0) {
                links = document.createElement('div');
                for (var i = 0; i < l; i++) {
                    var data = features[i].get('data');
                    var link = document.createElement('a');
                    var hasDescription = document.getElementById(data.id + "_description") !== null;
                    link.setAttribute('href', 'javascript: void(0)');
                    if (hasDescription) {
                        link.setAttribute('class', 'has_description');
                    }
                    link.appendChild(document.createTextNode(data.title));
                    if (map !== undefined) {
                        link.addEventListener('click', (function (d, map) {
                            return function () {
                                if (maps.OsmUtils.updateDescription(d['id'], map, pointData)) {
                                    maps.OsmUtils.toggleDescription(true);
                                }
                                maps.OsmUtils.zoomTo(map, d.lon + ";" + d.lat);
                                map.updateSize();
                            };
                        })(data, map));
                    }
                    links.appendChild(link);
                    if (i < l - 1) {
                        links.appendChild(document.createTextNode(', '));
                    }
                }
                if (l > 1) {
                    return links;
                }
            }
            var data = features[0].get('data');
            id = data.id;
            if (maps.OsmUtils.updateDescription(data['id'], map, pointData)) {
                maps.OsmUtils.toggleDescription(true);
            }
            maps.OsmUtils.setCenter(map, data);
            map.updateSize();
            return null;
        },
        updateView: function (map, params) {
            if ((params["lat"] !== null && Math.abs(params["lat"]) <= 90) &&
                    (params["lon"] !== null && params["lat"] <= 180) &&
                    (params["zoom"] !== null && params["zoom"] > 0)) {
                map.getView().setZoom(params["zoom"]);
                maps.OsmUtils.setCenter(map, params);
                return true;
            }
            return false;
        }
    };
})(this);