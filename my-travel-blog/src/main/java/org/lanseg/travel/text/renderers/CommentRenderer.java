package org.lanseg.travel.text.renderers;

import org.lanseg.travel.text.Tag;
import org.lanseg.travel.text.TagRenderer;

/**
 *
 * @author lans
 */
public class CommentRenderer implements TagRenderer {

    @Override
    public String render(Tag tag) {
        String comment = tag.getArguments().isEmpty() ? "" : tag.getArguments().get(0);
        return tag.isClosing() ? "</span>"
                : String.format("<span title='%s' class='editor_comment'>", comment);
    }

}
