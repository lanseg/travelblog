package org.lanseg.travel.orm;

import java.util.List;
import org.lanseg.travel.orm.dao.User;

/**
 *
 * @author lans
 */
public interface UserMapper {

    User getUser(String id);
    User getUserByCredentials(String username, String password);
    void addUser(User user);
    void deleteUser(String id);
    List<User> getUsers();
    int getUserCount();
    boolean userExists(String username, String password);
    List<String> getRoles(String userId);
    void addRole(String userId, String role);
    void removeRole(String userId, String role);
    void updateUser(User user);
}
