package org.lanseg.travel.orm.dao;

/**
 *
 * @author lans
 */
public class PictureInfo {

    private int id;
    private int pictureId;
    private long authorId;
    private boolean showAuthor;

    public PictureInfo() {
    }
    
    public PictureInfo(int id, int pictureId, long authorId) {
        this.id = id;
        this.pictureId = pictureId;
        this.authorId = authorId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public boolean isAuthorShowing() {
        return showAuthor;
    }

    public void setShowAuthor(boolean showAuthor) {
        this.showAuthor = showAuthor;
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PictureInfo)) {
            return false;
        }

        PictureInfo pi = (PictureInfo) o;
        return pi.pictureId == pictureId
                && pi.authorId == authorId
                && pi.id == id;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        hash = 79 * hash + this.pictureId;
        hash = 79 * hash + (int) (this.authorId ^ (this.authorId >>> 32));
        hash = 79 * hash + (showAuthor ? 1231 : 1237);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("PictureInfo {id:%d, pictureId:%d, author:%d, showAuthor: %b}", id,
                pictureId, authorId, showAuthor);
    }

}
