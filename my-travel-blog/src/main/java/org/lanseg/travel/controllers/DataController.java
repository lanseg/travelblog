package org.lanseg.travel.controllers;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import org.lanseg.travel.orm.PictureMapper;
import org.lanseg.travel.orm.PointMapper;
import org.lanseg.travel.orm.RecordMapper;
import org.lanseg.travel.orm.TrackMapper;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.Point;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Track;
import org.lanseg.travel.utils.Utils;
import org.lanseg.travel.utils.storage.Storage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author lans
 */
@Controller
public class DataController {

    private static final Logger LOG = Logger.getLogger("DataController");
    private static final String BACKGROUND_IMAGE = "bg.jpg";

    @Resource
    private PointMapper pointMapper;
    @Resource
    private PictureMapper pictureMapper;
    @Resource
    private RecordMapper recordMapper;
    @Resource
    private TrackMapper trackMapper;
    @Resource
    private Storage storage;

    
    @RequestMapping("/data/points") 
    public @ResponseBody List<Point> getPoints() {
        return pointMapper.getAllPoints(Utils.isEditor());
    }
    
    @RequestMapping("/data/{tripId}/points")
    public @ResponseBody List<Point> getPoints(@PathVariable("tripId") String tripId) {
        return pointMapper.getPoints(tripId, Utils.isEditor());
    }

    @RequestMapping("/data/{tripId}/tracks")
    public @ResponseBody List<Track> getTracks(@PathVariable("tripId") String tripId) {
        return trackMapper.getTripTracks(tripId);
    }
    
    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("/{tripId}/upload-header")
    public String uploadHeader(@PathVariable("tripId") String tripId,
            @RequestParam("file") MultipartFile file) {
        try {
            storage.uploadForTrip(tripId, BACKGROUND_IMAGE, file.getInputStream());
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Can't upload background for trip: {0}", ex.getMessage());
        }
        return "redirect:/" + tripId + "/edit";
    }

    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("/{tripId}/delete-background")
    public String deleteBackground(@PathVariable("tripId") String tripId) {
        storage.removeForTrip(tripId, BACKGROUND_IMAGE);
        return "redirect:/" + tripId + "/edit";
    }

    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("/{trip}/{record}/upload")
    public String uploadPhotos(@PathVariable("trip") String tripId,
            @PathVariable("record") String recordId,
            @RequestParam("file") List<MultipartFile> files) {
        try {
            Record r = recordMapper.getRecord(tripId, recordId);
            storage.uploadPhotos(tripId, r.getShortcut(), r.getId(), files).get()
                    .forEach(pictureMapper::addPicture);
        } catch (InterruptedException | ExecutionException ex) {
            LOG.log(Level.WARNING, "Failed to upload pictures: {0}.", ex.getMessage());
        }
        return "redirect:/" + tripId + "/" + recordId + "/edit";
    }

    @PreAuthorize("hasAuthority('editor')")
    @RequestMapping("{tripId}/{recordId}/edit_picture")
    public String editPicture(
            @PathVariable("tripId") String tripId,
            @PathVariable("recordId") String recordId,
            @RequestParam("id") int id,
            @RequestParam("action") String action) {
        if (action.equals("rotate")) {
            Picture picture = pictureMapper.getPicture(id);
            storage.rotatePicture(tripId, picture);
        }
        return "redirect:/" + tripId + "/" + recordId + "/edit";
    }
}
