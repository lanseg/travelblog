<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/WEB-INF/jspf/header.jspf" %>
<div class="container blog">
    <div class="blog-header" style="background-image: url(/files/${trip.id}/bg.jpg); ">
        <sec:authorize access="hasRole('editor')">
            <div class="editable">
                <a href="edit"><span class="glyphicon glyphicon-pencil"></span></a>
            </div>
        </sec:authorize>        
        <h1 class="blog-title">
            <c:if test="${not empty trip}">
                ${trip.title}
                <c:if test="${trip.draft}">
                    (черновик)
                </c:if>
            </c:if>
        </h1>
    </div>
    <h1>
        <c:choose>
            <c:when test="${empty trip}">
                <s:message code="record.not_found" />
            </c:when>
            <c:otherwise>
                <s:message code="record.draft" />
            </c:otherwise>
        </c:choose>
    </h1>
</div>           
<%@include file="/WEB-INF/jspf/footer.jspf" %>