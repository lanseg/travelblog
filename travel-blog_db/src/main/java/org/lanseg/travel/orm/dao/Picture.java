package org.lanseg.travel.orm.dao;

import java.util.Objects;

/**
 *
 * @author lans
 */
public class Picture {

    private int id;
    private int sortOrder;
    private int recordId;
    private String title;
    private String path;
    private String thumbnail;
    private PictureInfo info;
    
    public Picture() {
    }

    public Picture(int id, int sortOrder, int recordId, String title, String path,
            String thumbnail) {
        this.id = id;
        this.recordId = recordId;
        this.title = title;
        this.path = path;
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getSortOrder() {
        return this.sortOrder;
    }

    public PictureInfo getInfo() {
        return info;
    }

    public void setInfo(PictureInfo info) {
        this.info = info;
    }
    
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Picture)) {
            return false;
        }
        Picture a = (Picture) o;

        return a.id == id && a.path.equals(path) && recordId == a.recordId
                && ((title == null && a.title == null) || title.equals(a.title))
                && ((thumbnail == null && a.thumbnail == null) || thumbnail.equals(a.thumbnail));

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id;
        hash = 79 * hash + Objects.hashCode(this.recordId);
        hash = 79 * hash + Objects.hashCode(this.title);
        hash = 79 * hash + Objects.hashCode(this.path);
        hash = 79 * hash + Objects.hashCode(this.thumbnail);
        hash = 79 * hash + Objects.hash(this.info);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Picture {id: %d, recordId: %s, path: %s, title: %s, info: %s}",
                id, recordId, path == null ? "null" : path,
                title == null ? "null" : title,
                info == null ? "null" : info);
    }
}
