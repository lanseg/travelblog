package org.lanseg.travel.orm.dao;

import java.util.List;

/**
 *
 * @author lans
 */
public class TravelUser {
    
    private User user;
    private List<String> roles;

    public TravelUser(){}

    public TravelUser(User user, List<String> roles) {
        this.user = user;
        this.roles = roles;
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    
    
}
