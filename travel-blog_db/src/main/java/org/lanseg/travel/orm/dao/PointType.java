package org.lanseg.travel.orm.dao;

import java.util.Objects;

/**
 *
 * @author lans
 */
public class PointType {

    private int id;
    private String type;

    public PointType() {
    }

    public PointType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PointType)) {
            return false;
        }
        PointType t = (PointType) o;
        return t.getId() == id && t.getType().equals(type);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.id;
        hash = 43 * hash + Objects.hashCode(this.type);
        return hash;
    }
    
    @Override
    public String toString() {
        return String.format("PointType {id: %d, type: %s}", id, type);
    }

}
