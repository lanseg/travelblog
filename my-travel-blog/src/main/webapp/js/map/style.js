(function (root) {
    "use strict";
    root.maps = root.maps || {};
    root.maps.style = {
        defaults: {
            fontName: 'Sans',
            fontSize: 14,
            fontColor: 'black',
            fontStrokeColor: 'white',
            fontStrokeWidth: 2,
            pointColor: '#0FA3ED',
            pointRadius: 7,
            pointSelectedColor: '#FF0000',
            pointSelectedRadius: 7,
            pointStrokeColor: 'white',
            trackStrokeColor: '#005555',
            trackStrokeWidth: 2,
            trackSelectedStrokeColor: '#FF9988',
            trackSelectedStrokeWidth: 3
        },
        icons: {
            "bouddha": [160, 74], "apartment-3": [0, 37], "aircraftsmall": [96, 0],
            "snakes": [0, 259], "hiking": [0, 148], "battlefield": [256, 37],
            "chapel-2": [64, 111], "radiation": [224, 185], "dolphins": [192, 111],
            "deer": [160, 111], "bulldozer": [256, 74], "riparianhabitat": [160, 222],
            "japanese-food": [32, 148], "rodent": [192, 222], "cave-2": [32, 111],
            "ant-export": [256, 0], "agritourism": [64, 0], "theravadapagoda": [160, 259],
            "glacier-2": [288, 111], "sandwich-2": [224, 222], "art-museum-2": [128, 37],
            "boat": [128, 74], "restaurant_breakfast": [256, 185], "spider": [32, 259],
            "japanese-lantern": [64, 148], "police": [160, 185], "seals": [256, 222],
            "bridge_modern": [224, 74], "army": [96, 37], "alligator": [224, 0],
            "restaurant_chinese": [288, 185], "campfire-2": [288, 74], "japanese-temple": [96, 148],
            "memorial": [128, 148], "museum_archeological": [192, 148], "shintoshrine": [288, 222],
            "restaurant_steakhouse": [128, 222], "museum_war": [96, 185], "museum_naval": [0, 185],
            "treasure-mark": [224, 259], "anthropo": [288, 0], "restaurant_korean": [32, 222],
            "mountains": [160, 148], "coffee": [96, 111], "sumo-2": [128, 259],
            "drinkingwater": [224, 111], "atm-2": [160, 37], "photography": [128, 185],
            "carrental": [0, 111], "museum_science": [64, 185], "aboriginal": [0, 0],
            "construction": [128, 111], "steamtrain": [96, 259], "aquarium": [32, 37],
            "zoo": [32, 296], "museum_art": [224, 148], "restaurant_mexican": [64, 222],
            "birds-2": [96, 74], "museum_crafts": [256, 148], "algae": [192, 0],
            "bank": [192, 37], "bread": [192, 74], "archery": [64, 37],
            "fastfood": [256, 111], "publicart": [192, 185], "bar": [224, 37],
            "museum_industry": [288, 148], "air_fixwing": [128, 0], "restaurant": [96, 222],
            "workshop": [0, 296], "university": [256, 259], "museum_openair": [32, 185],
            "statue-2": [64, 259], "airport": [160, 0], "train": [192, 259],
            "bigcity": [64, 74], "bed_breakfast1-2": [32, 74], "beautifulview": [0, 74],
            "battleship-3": [288, 37], "accesdenied": [32, 0], "restaurant_greek": [0, 222],
            "volcano-2": [288, 259]}
    };
})(this);