package org.lanseg.travel.text.renderers;

import org.lanseg.travel.text.Tag;
import org.lanseg.travel.text.TagRenderer;

/**
 *
 * @author lans
 */
public class PointRenderer implements TagRenderer {

    private final boolean editorMode;
    
    public PointRenderer(boolean editorMode) {
        this.editorMode = editorMode;
    }

    
    @Override
    public String render(Tag tag) {
        if (tag.isClosing()) {
            return "</div>";
        }        
        if (tag.getArguments().isEmpty()) {
            return editorMode ? "<div class='warning'>Empty IMG tag" : "<div>";
        }
        if (editorMode) {
            return String.format("<div id='%s_description' title='%s' class='point_description editor_comment'>",
                    tag.getArguments().get(0), tag.getArguments().get(0));
        }
        return String.format("<div id='%s_description' class='point_description'>",
                tag.getArguments().get(0));
    }

}
