package org.lanseg.travel.orm;

import java.util.List;
import org.lanseg.travel.orm.dao.Record;

/**
 *
 * @author lans
 */
public interface RecordMapper {

    void addRecord(Record record);
    Record getRecord(String trip, String shortcut);
    Record getRecordById(int recordId);
    void updateRecord(Record r);
    void deleteRecord(int id);
    int getRecordCount(String trip);
    List<Record> getRecords(String trip, boolean showAll);
    List<Record> getRecordsNoBody(String trip, boolean showAll);
}
