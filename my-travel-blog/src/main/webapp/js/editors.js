/*jslint browser: true*/
/*jslint white: true */
/*global $, jQuery, ol, tripId, csrfToken, csrfHeader, social, gui */

function appendCSRF(request) {
    if (csrfToken !== undefined &&
            csrfHeader !== undefined) {
        request.setRequestHeader(csrfHeader, csrfToken);
    }
}

var setEditable = function (node, callback) {
    var editor = document.createElement('input'),
            prevState = node.style.display;
    editor.setAttribute("type", "text");
    editor.setAttribute("value", node.innerHTML);
    editor.style.display = "none";
    node.parentNode.insertBefore(editor, node.nextSibling);
    node.addEventListener("click", function (e) {
        e.preventDefault();
        e.stopPropagation();
        node.style.display = "none";
        editor.style.display = "block";
        editor.focus();
    });
    editor.addEventListener("blur", function (e) {
        node.style.display = prevState;
        editor.style.display = "none";
        callback(editor.value);
    });
    editor.addEventListener("keyup", function (e) {
        var code = e.keyCode;
        if (code === 27 || code === 13) {
            node.style.display = prevState;
            editor.style.display = "none";
            if (code === 13) {
                callback(editor.value);
            }
        }
    });
};

var setEditableAjax = function (node, params) {
    return setEditable(node, function (value) {
        $.ajax({
            type: "POST",
            url: params.url,
            beforeSend: appendCSRF,
            data: {
                id: params.id,
                field: params.field,
                value: value
            },
            success: function (data) {
                node.innerHTML = data[params.field];
            }
        });
    });
};
function insertAtCursor(item, text) {
    var startPos = item.selectionStart;
    var endPos = item.selectionEnd;
    item.value = item.value.substring(0, startPos)
            + text
            + item.value.substring(endPos, item.value.length);
    item.selectionStart = startPos + 1;
    item.selectionEnd = endPos + 1;
}
function getPos(el) {
    for (var lx = 0, ly = 0; el !== null;
            lx += el.offsetLeft,
            ly += el.offsetTop, el = el.offsetParent);
    return {x: lx, y: ly};
}
(function () {
    window.onload = function () {
        "use strict";
        var lastFocus;
        gui.DnD(".pictures .picture");

        $(".date input").datepicker($.datepicker.regional[ "ru" ]);
        $(".picture .title .text a").each(function () {
            setEditableAjax(this, {
                url: "/" + tripId + "/update_picture",
                field: "title",
                id: this.parentNode.parentNode.parentNode.id.replace("picture_", "")
            });
        });
        $(".uploader.tracks table tbody .title a").each(function () {
            console.log(this);
            var trackId = this.parentNode.parentNode.id.replace("track_", "");
            setEditableAjax(this, {
                url: "/" + tripId + "/tracks/" + trackId + "/update",
                field: "title"
            });
        });
        $(".toggler").on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var toggler = this;
            $.get(toggler.getAttribute('href'), function (data) {
                toggler.innerHTML = data;
            });
        });
        $(".points .item-edit").on('click', function (evt) {
            var row = $(this).parent().parent().find("td");
            var len = row.length;
            row.each(function (index) {
                if (index < len - 1) {
                    $(this).html("<input type='text' name='" + $(this).attr('class') + "' value='" + $(this).text() + "'>");
                } else {
                    $(this).find(".item-apply").show();
                }
            });
            $(this).hide();
        });
        $(".points .item-apply").on('click', function () {
            var result = {};
            var tr = $(this).parent().parent();
            var row = tr.find("input");
            row.each(function () {
                result[this.name] = this.value;
            });
            $.ajax({
                type: "POST",
                url: "/" + tripId + "/update_point",
                data: result,
                beforeSend: appendCSRF,
                success: function (data) {
                    tr.children().each(function (i) {
                        var item = $(this);
                        item.text(data[item.attr('class')]);
                    });
                    tr.find('.item-apply').hide();
                    tr.find('.item-edit').show();
                }
            });
        });
        $("#body").blur(function () {
            lastFocus = this;
        });
        $(".pictures .picture .thumbnail").on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var id = $(this).parent().attr('id').replace('picture_', '');
            var textarea = document.getElementById("body");
            insertAtCursor(textarea, "[img " + id + "]");
            if (lastFocus) {
                lastFocus.focus();
            }
        });
        $(".row.charmap span").on('click', function (e) {
            var textarea = document.getElementById("body");
            insertAtCursor(textarea, this.textContent);
        });
        $(".header_toggler").on('click', function (e) {
            $(this).next().toggle();
        });
        var currentPoint = null;
        $("#icon_box span").on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();

            $.ajax({
                type: "POST",
                url: "/" + tripId + "/set_point_type",
                beforeSend: appendCSRF,
                data: {point: currentPoint, type: this.getAttribute('data-type')},
                success: function (data) {
                    var span = $('*[data-point="' + data.id + '"] span').get(0);
                    if (data.type !== null) {
                        span.setAttribute('class', 'map_icons ' + data.type.type);
                        span.innerHTML = '';
                    } else {
                        span.setAttribute('class', '');
                        span.innerHTML = 'NULL';
                    }
                }
            });
        });

        $(".icon_chooser").on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var pos = getPos(this), box = $('#icon_box'),
                    span = $(this).find('.map_icons').get(0),
                    current;
            if (span !== undefined) {
                current = $(box).find('.' + span.getAttribute('class').replace(' ', '.'));
                current.css({'border': 'solid 1px red'});
            }
            box.hide();
            box.css({'top': pos.y + 37 / 2, 'left': pos.x});
            if (currentPoint !== this.getAttribute('data-point')) {
                box.show('fast');
                currentPoint = this.getAttribute('data-point');
            }
        });

        setEditable(document.getElementById("vk_user"), function (value) {
            social.vk.initPhotoChooser(value);
        });
    };
})();