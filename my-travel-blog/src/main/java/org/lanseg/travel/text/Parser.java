package org.lanseg.travel.text;

/**
 *
 * @author lans
 */
public abstract class Parser {

    private final String data;

    public Parser(String data) {
        this.data = data;
    }

    public void parse() {
        StringBuilder content = new StringBuilder();
        StringBuilder buffer = new StringBuilder();
        int braces = 0;
        for (int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            if (c == '[') {
                braces++;
                if (content.length() > 0) {
                    onContent(content.toString());
                    content.setLength(0);
                }
            }
            if (braces > 0) {
                buffer.append(c);
            } else {
                content.append(c);
            }
            if (c == ']' && braces > 0) {
                braces--;
                if (braces == 0) {
                    onTag(new Tag(buffer.toString()));
                    buffer.setLength(0);
                }
            }
        }
        if (content.length() > 0) {
            onContent(content.toString());
        }
        onEnd();
    }

    public abstract void onTag(Tag tag);

    public abstract void onContent(String content);
    
    public abstract void onEnd();
}
