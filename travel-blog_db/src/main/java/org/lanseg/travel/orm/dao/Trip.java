package org.lanseg.travel.orm.dao;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author lans
 */
public class Trip {

    private String id;
    private String title;
    private String shortTitle;
    private String description;
    private Date startDate;
    private Date endDate;
    private boolean draft;

    public Trip() {
    }

    public Trip(String id, String title, String shortTitle, String description, Date startDate,
            Date endDate) {
        this.id = id;
        this.title = title;
        this.shortTitle = shortTitle;
        this.description = description;
        this.startDate = startDate == null ? null : new Date((startDate.getTime() / 1000) * 1000);
        this.endDate = endDate == null ? null : new Date((endDate.getTime() / 1000) * 1000);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate == null ? null : new Date((startDate.getTime() / 1000) * 1000);
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate == null ? null : new Date((endDate.getTime() / 1000) * 1000);
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    @Override
    public String toString() {
        return String.format("Trip {id: \"%s\", title: \"%12s\", description:\"%12s\", Since %s to %s)}",
                id == null ? "null" : id,
                title == null ? "null" : title.substring(0, Math.min(title.length(), 10)),
                description == null ? "null" : description.substring(0, Math.min(description.length(), 10)),
                startDate, endDate);
    }

    private boolean equalsOrNull(Object a, Object b) {
        return (a == null && b == null)
                || (a != null && b != null && a.equals(b));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Trip)) {
            return false;
        }
        Trip t1 = (Trip) o;
        return id.equals(t1.id)
                && t1.title.equals(title)
                && t1.shortTitle.equals(shortTitle)
                && equalsOrNull(description, t1.description)
                && equalsOrNull(startDate, t1.startDate)
                && equalsOrNull(endDate, t1.endDate);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.title);
        hash = 83 * hash + Objects.hashCode(this.description);
        hash = 83 * hash + Objects.hashCode(this.startDate);
        hash = 83 * hash + Objects.hashCode(this.endDate);
        return hash;
    }
}
