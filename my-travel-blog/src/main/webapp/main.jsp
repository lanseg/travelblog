<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div id="map_holder" class="page map" style="z-index: 0;">
    <div id="point_description" class="map_sidebar">
        <div id="hide_pd" class="hide_button">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
        </div>
        <div class="blog-post diary">
            <h2 class="caption">
                <s:message code="map.point_description" />
            </h2>
            <div class="content"></div>
        </div>
    </div>
    <div id="map" class="map full">
        <img src="/img/crosshair.png" class="crosshair"/> 
    </div>           
    <sec:authorize access="hasAuthority('ROLE_ANONYMOUS')">
        <div id="point_popup" class="point_popup"></div>
    </sec:authorize>        
</div>
<div class="page diary diary_trips"> 
    <div class="container blog">
        <center>
            <div class="trips_grid">
                <div class="cards">
                    <c:forEach var="trip" items="${trips}" varStatus="loop">
                        <div class="card">                            
                            <a href="/${trip.id}/">
                                <div class="picture">
                                    <div class="border">
                                        <img src="/files/${trip.id}/bg.jpg" />
                                    </div>
                                    <div class="title">${trip.title}</div>
                                    <div class="description">${trip.description}</div>                                    
                                </div>
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </center>
    </div>
</div>
<%@include file="/WEB-INF/jspf/footer.jspf" %>