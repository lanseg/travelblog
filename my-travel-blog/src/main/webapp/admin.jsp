<%@page import="org.lanseg.travel.utils.Utils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@include file="/WEB-INF/jspf/editor_header.jspf" %>
<script>
    window.onload = function () {
        $(".user a.editable").each(function () {
            var field = this.href.substring(this.href.indexOf('#') + 1);
            setEditableAjax(this, {
                url: "/admin/update_user",
                field: field,
                id: this.parentNode.parentNode.id
            });
        });
    };
</script>
<div class="content">
    <h2 class="header_toggler"><s:message code="admin.premissions" /></h2>
    <div class="data">
        <table class="table">
            <thead>
                <tr>           
                    <th>Username</th>                
                    <th>Password</th>      
                    <th>Permissions</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="user" items="${users}">
                    <tr class="user" id="${user.id}">
                        <td><a href="#username" class="editable">${user.username}</a></td>       
                        <td>
                            <a href="#password" class="editable">${user.password}</a>
                        </td>
                        <td>
                            <a href="#roles" class="editable"><c:forEach items="${user.roles}" var="role">${role} </c:forEach></a>
                            </td>
                            <td><a href="/admin/remove_user?id=${user.id}"><span class="glyphicon glyphicon-remove"></span></a></td>
                    </tr>
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td><input type="text" name="username" form="user_add_form"/></td>
                    <td><input type="text" name="password" form="user_add_form"/></td>
                    <td><input type="text" name="roles" form="user_add_form"/></td>
                    <td>
                        <button form="user_add_form"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
                    </td>
                </tr>
            </tfoot>
        </table>
        <form method="POST" action="/admin/add_user" id="user_add_form">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
    </div>
</div>
<div class="content">
    <h2 class="header_toggler"><s:message code="admin.point_types" /></h2>
    <div class="data">
        <table class="table">
            <thead>
                <tr>  
                    <th><s:message code="admin.icon" /></th>
                    <th>id</th>                
                    <th><s:message code="admin.type" /></th>     
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="type" items="${pointTypes}">
                    <tr>
                        <td><span class="map_icons ${type.type}"></span></td>
                        <td>${type.id}</td>
                        <td>${type.type}</td>
                        <td><a href="/admin/remove_point_type?id=${type.id}"><span class="glyphicon glyphicon-remove"></span></a></td>
                    </tr>
                </c:forEach>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td><input type="text" name="point_type" form="ptype_add_form"/></td>
                    <td>
                        <button form="ptype_add_form"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
                    </td>
                </tr>
            </tfoot>        
        </table>
        <form method="POST" action="/admin/add_point_type" id="ptype_add_form">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>    
    </div>
</div>
<div class="content">
    <h2 class="header_toggler"><s:message code="admin.local_storage" /></h2>
    <div class="data">
        <div>${storageRoot}</div>
        <div>${Utils.formatFileSize(localFilesSize)} в ${localFiles.size()} ${Utils.plural(localFiles.size(), "файле", "файлах", "файлах")}</div>    
        <div style="overflow-y: scroll; height: 500px;">
            <table class="table files">
                <thead>
                    <tr>
                        <th><s:message code="admin.path" /></th>  
                        <th><s:message code="admin.created_at" /></th>                
                        <th><s:message code="admin.size" /></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="file" items="${localFiles}">
                        <tr>
                            <td>${Utils.formatFileSize(file.value.size())}</td>
                            <td>${Utils.formatFileTime(file.value.creationTime())}</td>
                            <td>${file.key}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/jspf/editor_footer.jspf" %>