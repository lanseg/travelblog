package org.lanseg.travel.text.renderers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.PictureInfo;
import org.lanseg.travel.orm.dao.Trip;
import org.lanseg.travel.text.Tag;
import org.lanseg.travel.text.TagRenderer;
import org.lanseg.travel.text.TripContext;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 *
 * @author lans
 */
public class ImgRenderer implements TagRenderer {

    private static final String CONTROLS = "<div class=\"controls\">"
            + "    <div class=\"prev\">\u25c0</div>"
            + "    <div class=\"next\">\u25b6</div>"
            + "</div>";

    private final TripContext context;

    public ImgRenderer(TripContext context) {
        this.context = context;
    }

    public static String render(Trip trip, List<Picture> items, String id) {
        StringBuilder result = new StringBuilder();
        if (items.isEmpty()) {
            return "<i>No such item</i>";
        }
        result.append("<div class=\"img-box\"><div class=\"content\">");
        if (items.size() > 1) {
            result.append(CONTROLS);
        }
        boolean first = true;
        for (Picture item : items) {
            String link;
            if (item.getPath().startsWith("http")) {
                link = item.getPath();
            } else {
                link = "/files/" + trip.getId() + "/" + item.getPath();
            }
            link = escapeHtml4(link);
            if (first) {
                result.append("<div class=\"gallery_item active\">");
                first = false;
            } else {
                result.append("<div class=\"gallery_item\">");
            }
            result.append(String.format("<a data-lightbox=\"%s\" href=\"%s\">", id, link));
            result.append(String.format("<img alt=\"%s\" data-src=\"%s\" />",
                    escapeHtml4(item.getTitle()), link));
            result.append("<div class=\"data-title\">");
            PictureInfo info = item.getInfo();
            if (info != null && info.isAuthorShowing()) {
                result.append(String.format("© <div class=\"author\" data-userid=\"%d\">"
                        + "http://vk.com/id%d</div>", info.getAuthorId(), info.getAuthorId()));
            }
            result.append("</div>");
            result.append("</a>");
            if (item.getTitle() != null) {
                result.append(String.format("<div class=\"caption\">%s</div>",
                        escapeHtml4(item.getTitle())));
            }
            result.append("</div>");
        }
        return result.append("</div></div>").toString();
    }

    @Override
    public String render(Tag tag) {
        Map<Integer, Picture> picturesById = context.getpicturesById();
        List<Picture> currentPictures = tag.getArguments().stream()
                .filter(a -> a.matches("\\d+"))
                .map((a) -> picturesById.get(Integer.valueOf(a)))
                .collect(Collectors.toList());
        return render(context.getTrip(), currentPictures, Long.toHexString((long) tag.hashCode()));
    }
}
