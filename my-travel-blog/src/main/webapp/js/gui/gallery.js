/* global items */

(function (root) {
    "use strict";
    root.gui = root.gui || {};
    root.gui.gallery = {
        swapImages: function (current, next) {
            current.animate({opacity: 0}, {duration: 200});
            current.hide();
            next.css('opacity', 0);
            next.show();
            next.animate({opacity: 1}, {duration: 200});
        },
        init: function (selector) {
            var swapImages = this.swapImages;
            $(selector).each(function () {
                var imgbox = this,
                        items = $(imgbox).find(".gallery_item").toArray(),
                        currentIndex = 0;
                $(this).find('.prev').on('click', function () {
                    var current, next;
                    currentIndex--;
                    if (currentIndex < 0) {
                        currentIndex = items.length - 1;
                        current = $(items[0]);
                        next = $(items[items.length - 1]);
                    } else {
                        current = $(items[currentIndex + 1]);
                        next = $(items[currentIndex]);
                    }
                    swapImages(current, next);
                });
                $(this).find('.next').on('click', function () {
                    var current, next;
                    currentIndex++;
                    if (currentIndex === items.length) {
                        currentIndex = 0;
                        current = $(items[items.length - 1]);
                        next = $(items[0]);
                    } else {
                        current = $(items[currentIndex - 1]);
                        next = $(items[currentIndex]);
                    }
                    swapImages(current, next);
                });
            });
        }
    };
})(this);