/*jslint browser: true*/
/*jslint white: true */
/*global $, social, maps, settings, gui, Bus */
window.onload = function () {
    "use strict";
    var customMap = new maps.CustomMap(document.getElementById('map'), maps.style.defaults);
    customMap.initMap();
    var map = customMap.map;

    gui.gallery.init(".img-box .content");
    gui.scroll.init("links", settings);

    function hide(node) {
        node.style.display = 'none';
    }

    function show(node) {
        node.style.display = 'block';
    }

    function setPage(page) {
        var item = document.querySelector(page + "_toggle");
        document.querySelectorAll(".page").forEach(hide);
        document.querySelectorAll(".category").forEach(node => node.classList.remove('active'));
        document.querySelectorAll("." + item.getAttribute('href').substring(1)).forEach(show);
        item.classList.add('active');
        if (page === '#map') {
            map.updateSize();
            if (!maps.OsmUtils.updateView(map, settings.mapSettings) &&
                    customMap.bounds !== undefined) {
                map.getView().fit(customMap.bounds, map.getSize());
            }
        } else if (page === "#diary") {
            window.scrollTo(0, settings.scroll);
        }
        document.querySelectorAll(".trips a").forEach(node => {
            var ref = node.getAttribute('href');
            var hash = ref.indexOf('#');
            if (hash !== -1) {
                href = ref.substring(0, hash);
            }
            this.href = ref + page;
        });
        settings.setCurrentPage(page.substring(1));
    }
    function showSection(obj, e, section) {
        e.preventDefault();
        e.stopPropagation();
        var parent = $(obj).parent();
        var current = parent.find(".section." + section);
        if (current.css('display') !== "none") {
            current.hide('fast');
        } else {
            parent.find(".section").hide('fast');
            current.show('fast');
        }
        if (section === "comments") {
            social.vk.initComments(current.get(0));
        }
    }
    document.getElementById("hide_pd").addEventListener('click', function () {
        maps.OsmUtils.toggleDescription(false);
        map.updateSize();
    });
    document.querySelectorAll(".blog-post .footer .sections a.button").forEach(
            function (item) {
                item.addEventListener('click', function (e) {
                    showSection(this, e, this.getAttribute('href').substring(1));
                }.bind(item));
            });
    document.querySelectorAll(".category").forEach(node => node.addEventListener('click',
                function (e) {
                    e.preventDefault();
                    setPage($(this).attr('href'));
                }));
    document.querySelectorAll("a.poi").forEach(node => node.addEventListener('click',
                function (e) {
                    e.preventDefault();
                    setPage("#map");
                    maps.OsmUtils.zoomTo(map, $(this).attr('href'));
                }));
    document.querySelectorAll('.dropdown-menu input, .dropdown-menu button').forEach(node => node.addEventListener('click',
                function (e) {
                    e.stopPropagation();
                }));
    if (settings.currentPage === "diary" || settings.currentPage === "map") {
        setPage('#' + settings.currentPage);
    }
    document.querySelectorAll("img").forEach(function (img) {
        var src = img.dataset.src;
        if (src === undefined) {
            return;
        }
        img.onerror = function(err) {
            img.classList.add("empty_image");
        }.bind(img);
        img.setAttribute("src", src);
    });
    social.vk.init(true);
    social.vk.updateAuthors();
};
