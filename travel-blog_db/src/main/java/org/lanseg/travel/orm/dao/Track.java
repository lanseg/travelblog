package org.lanseg.travel.orm.dao;

import java.util.Objects;

/**
 *
 * @author lans
 */
public class Track {
    
    private int id;
    private String filename;
    private String title;
    private String tripId;
    private Integer recordId;

    public Track() {}
    
    public Track (int id, String filename, String title, String tripId, Integer recordId) {
        this.id = id;
        this.filename = filename;
        this.title = title;
        this.tripId = tripId;
        this.recordId = recordId;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }
    
    @Override
    public String toString() {
        return String.format("Track {id: %d, file: %s, title: %s, tripId: %s, recordId: %d}", 
                id, filename, title, tripId, recordId);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Track)) {
            return false;
        }
        Track t = (Track) obj;
        return id == t.id &&
                filename.equals(t.filename) &&
                ((title == null && t.title == null) || title.equals(t.title)) &&
                ((recordId == null && t.recordId == null) || recordId.equals(t.recordId)) &&
                tripId.equals(t.tripId);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.filename);
        hash = 83 * hash + Objects.hashCode(this.title);
        hash = 83 * hash + Objects.hashCode(this.tripId);
        hash = 83 * hash + this.recordId;
        return hash;
    }
    
    
}
