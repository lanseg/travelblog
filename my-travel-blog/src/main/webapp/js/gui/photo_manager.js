/*jslint browser: true*/
/*jslint white: true */
/*global $, jQuery, tripId, csrfToken, csrfHeader, gui */
(function (root) {
    "use strict";
    root.gui = root.gui || {};
    root.gui.photos = {
        allPhotos: {},
        selectedPhotos: {},
        getBySize: function (variants, size) {
            for (var i = 0; i < variants.length; i++) {
                if (variants[i]["type"] === size) {
                    return variants[i]["url"];
                }
            }
            return null;
        },
        getLargestPhoto: function(variants) {
            var sum = 0;
            var result = null;
            for (var i = 0; i < variants.length; i++) {
                var newSum = variants[i]["width"] + variants[i]["height"];
                if (newSum > sum) {
                    sum = newSum;
                    result = variants[i]["url"];
                }
            }
            return result;
        },
        uploadPhotos: function (record, photos, callback) {
            $.ajax({
                url: "/" + tripId + "/" + record + "/add_photos",
                type: "POST",
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify(photos),
                beforeSend: function (request) {
                    //appendCSRF
                    if (csrfToken !== undefined && csrfHeader !== undefined) {
                        request.setRequestHeader(csrfHeader, csrfToken);
                    }
                },
                success: function (result) {
                    if (callback) {
                        callback(result);
                    }
                },
                failure: function (result) {
                    alert(JSON.stringify(result));
                }
            });
        },
        addPhotos: function (record) {
            if (record !== "") {
                var photosToAdd = [];
                var selectedPhotos = this.selectedPhotos;
                for (var p in selectedPhotos) {
                    if (selectedPhotos.hasOwnProperty(p)) {
                        var src = gui.photos.getLargestPhoto(selectedPhotos[p]["sizes"]);
                        photosToAdd.push({
                            path: src,
                            thumbnail: gui.photos.getBySize(selectedPhotos[p]["sizes"], "m"),
                            info: {
                                showAuthor: document.getElementById("add_info").checked ? true : false,
                                authorId: selectedPhotos[p]["owner_id"]
                            }
                        });
                    }
                }
                this.uploadPhotos(record, photosToAdd, function (e) {
                    //Just to indicate.
                    alert("Photos loaded");
                });
            }
        },
        showPhotos: function (photos) {
            var i, gallery = document.getElementById('remote_gallery');
            while (gallery.firstChild) {
                gallery.removeChild(gallery.firstChild);
            }
            for (i = 0; i < photos["count"]; i++) {
                var p = photos["items"][i];
                //TODO: replace with this?
                gui.photos.allPhotos[p["id"]] = p;
                gallery.appendChild(gui.photos.createSelectableImage(gui.photos.getBySize(p["sizes"], "m"), p.id));
            }
        },
        selectPhoto: function (id) {
            var newPhoto = this.createSelectableImage(gui.photos.getBySize(this.allPhotos[id]["sizes"], "m"), id, true);
            document.getElementById('selected_photos').appendChild(newPhoto);
            this.selectedPhotos[id] = this.allPhotos[id];
        },
        deselectPhoto: function (id) {
            if (id in this.selectedPhotos) {
                document.getElementById('selected_photos').removeChild(
                        document.getElementById('selected_' + id));
                delete this.selectedPhotos[id];
            }
        },
        createSelectableImage: function (src, id, checked) {
            var base = document.createElement('div'),
                    thumbnail = document.createElement('div'),
                    info = document.createElement('div'),
                    link = document.createElement('a'),
                    img = document.createElement('img'),
                    checker = document.createElement("input"),
                    that = this;
            base.className = 'picture';
            thumbnail.className = 'thumbnail';
            info.className = 'info';
            link.setAttribute('href', src);
            img.setAttribute('src', src);
            checker.setAttribute('type', 'checkbox');
            checker.setAttribute('data-image-id', id);
            if (arguments.length === 3 && checked) {
                base.setAttribute('id', 'selected_' + id);
                checker.setAttribute('checked', 'checked');
            } else {
                base.setAttribute('id', 'base_' + id);
            }
            info.appendChild(checker);
            link.appendChild(img);
            thumbnail.appendChild(info);
            thumbnail.appendChild(link);
            base.appendChild(thumbnail);
            checker.addEventListener('change', function (e) {
                var id = this.getAttribute('data-image-id');
                if (this.checked) {
                    that.selectPhoto(id);
                } else {
                    that.deselectPhoto(id);
                }
            });
            return base;
        }
    };
})(this);