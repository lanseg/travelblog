package org.lanseg.travel.orm;

import java.util.List;
import org.lanseg.travel.orm.dao.Trip;

/**
 *
 * @author lans
 */
public interface TripMapper {
    
    int addTrip(Trip t);
    void updateTrip(Trip t);
    Trip getTrip(String id);
    List<Trip> getTrips(boolean showAll);
    void deleteTrip(String id);
}
