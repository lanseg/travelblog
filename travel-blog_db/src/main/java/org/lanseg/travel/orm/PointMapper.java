package org.lanseg.travel.orm;

import java.util.List;
import org.lanseg.travel.orm.dao.Point;
import org.lanseg.travel.orm.dao.PointType;

/**
 *
 * @author lans
 */
public interface PointMapper {
    
    void addPoints(List<Point> p);
    void deletePoint(String pointId);
    void updatePoint(Point point);
    Point getPoint(String pointId);
    List<Point> getPoints(String tripId, boolean showAll);
    List<Point> getAllPoints(boolean showAll);
    List<Point> getPointsForRecord(int recordId);
    void addPointType(PointType type);
    void setPointType(String pointId, Integer typeId);
    void deletePointType(int typeId);
    List<PointType> getPointTypes();
}
