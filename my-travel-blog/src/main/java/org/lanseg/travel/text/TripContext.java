package org.lanseg.travel.text;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.Point;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Trip;
import org.lanseg.travel.utils.Utils;

/**
 *
 * @author lans
 */
public class TripContext {
    
    private final Trip trip;
    private final List<Record> records;
    private final List<Picture> pictures;
    private final List<Point> points;
    
    public TripContext (Trip trip, List<Record> records, List<Picture> pictures, List<Point> points) {
        this.trip = trip;
        this.records = Collections.unmodifiableList(records);
        this.pictures = Collections.unmodifiableList(pictures);
        this.points = Collections.unmodifiableList(points);
    }

    public List<Record> getRecords() {
        return records;
    }
    
    public List<Picture> getpictures() {
        return pictures;
    }
    
    public Map<Integer, Picture> getpicturesById() {
        return Utils.listToMap(pictures, Picture::getId);
    }
    
    public List<Point> getPoints() {
        return points;
    }

    public Trip getTrip() {
        return trip;
    }
}
