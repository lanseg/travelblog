package org.lanseg.travel.text;

import java.util.ArrayDeque;
import org.lanseg.travel.text.renderers.PointRenderer;
import java.util.HashMap;
import java.util.Map;
import org.lanseg.travel.text.renderers.CommentRenderer;
import org.lanseg.travel.text.renderers.ErrorRenderer;
import org.lanseg.travel.text.renderers.ImgRenderer;

/**
 *
 * @author lans
 */
public class Processor {

    private final TripContext context;
    private final Map<String, TagRenderer> renderers;

    public Processor(TripContext context, boolean editorMode) {
        this.context = context;
        this.renderers = new HashMap<>();

        renderers.put("img", new ImgRenderer(context));
        renderers.put("point", new PointRenderer(editorMode));
        if (editorMode) {
            renderers.put("comment", new CommentRenderer());
            renderers.put("error", new ErrorRenderer());
        }
    }

    //TODO: Nested tag support
    private static String processText(String text, Map<String, TagRenderer> renderers) {
        StringBuilder result = new StringBuilder();
        (new Parser(text) {

            private final ArrayDeque<Tag> tagTree = new ArrayDeque<>();

            private String render(Tag tag) {
                TagRenderer renderer = renderers.get(tag.getName());
                return renderer == null ? "" : renderer.render(tag);
            }

            private String renderError(Tag tag) {
                TagRenderer renderer = renderers.get("error");
                return renderer == null ? "" : renderer.render(tag);
            }

            @Override
            public void onTag(Tag tag) {
                if (!tag.isClosing()) {
                    if (!tag.isSelfClosing()) {
                        tagTree.push(tag);
                    }
                    result.append(render(tag));
                } else {
                    Tag related = tagTree.poll();
                    if (tag.isClosingOf(related)) {
                        result.append(render(tag));
                    } else {
                        result.append(renderError(tag));
                    }
                }
            }

            @Override
            public void onContent(String content) {
                result.append(content);
            }

            @Override
            public void onEnd() {
                tagTree.forEach((tag) -> result.append(render(new Tag(String.format("[/%s]", tag.getName())))));
            }

        }).parse();
        return result.toString();
    }

    public void process() {
        context.getRecords().forEach(r -> r.setBody(processText(r.getBody(), renderers)));
    }

}
