package org.lanseg.travel.db.test;

import java.util.ArrayList;
import java.util.List;
import org.lanseg.travel.orm.TrackMapper;
import static org.junit.Assert.*;
import org.junit.Test;
import org.lanseg.travel.orm.dao.Track;

/**
 *
 * @author lans
 */
public class TrackMapperTest extends TestBase {

    private final TrackMapper trackMapper;
    
    public TrackMapperTest() {
        trackMapper = session.getMapper(TrackMapper.class);
    }
    
    @Test
    public void testSimpleAddRemove() {
        Track track = new Track(0, "test.json", "Test track", testTrip.getId(), testRecord.getId());
        trackMapper.addTrack(track);
        Track fromDb = trackMapper.getTrack(track.getId());
        trackMapper.deleteTrack(track.getId());
        assertEquals(track, fromDb);
        assertNull(trackMapper.getTrack(track.getId()));   
    }
    
    @Test
    public void testMultipleInsertRemove() {
        int tracksToCheck = 20;
        List<Track> tracks = new ArrayList<>(tracksToCheck);
        for (int i = 0; i < tracksToCheck; i++) {
            Track t = new Track(0, "track_" + i, "Track title " + i, testTrip.getId(),
                    testRecord.getId());
            trackMapper.addTrack(t);
            tracks.add(t);
        }
        List<Track> fromDbTrip = trackMapper.getTripTracks(testTrip.getId());
        List<Track> fromDbRecord = trackMapper.getRecordTracks(testRecord.getId());
        tracks.forEach(t -> trackMapper.deleteTrack(t.getId()));
        assertEquals("All tracks removed", 0, trackMapper.getTripTracks(testTrip.getId()).size());
        assertTrue("All trip tracks", tracks.containsAll(fromDbTrip));
        assertTrue("All record tracks", tracks.containsAll(fromDbRecord));
    }
}