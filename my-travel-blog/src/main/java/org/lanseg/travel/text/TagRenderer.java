package org.lanseg.travel.text;

/**
 *
 * @author lans
 */
public interface TagRenderer {
    
    String render(Tag tag);
}
