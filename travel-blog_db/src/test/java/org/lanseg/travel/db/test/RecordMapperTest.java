/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lanseg.travel.db.test;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import static org.junit.Assert.*;
import org.junit.Test;
import org.lanseg.travel.orm.RecordMapper;
import org.lanseg.travel.orm.TripMapper;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Trip;

/**
 *
 * @author lans
 */
public class RecordMapperTest {

    private static final String CONFIG = "org/lanseg/travel/orm/config.xml";
    private RecordMapper recordMapper;
    private TripMapper tripMapper;

    public RecordMapperTest() {
        try {
            SqlSession session = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream(CONFIG)).openSession(true);
            recordMapper = session.getMapper(RecordMapper.class);
            tripMapper = session.getMapper(TripMapper.class);
        } catch (IOException ex) {
        }
    }

    @Test
    public void testInsertRemove() {
        Trip firstTrip = new Trip("0", "Dummy title", "Dummy short title", "Dummy descr", null, null);
        Trip secondTrip = new Trip("1", "Dummy title", "Dummy short title", "Dummy descr", null, null);
        tripMapper.addTrip(firstTrip);
        tripMapper.addTrip(secondTrip);
        Record firstRecord = new Record(10000, firstTrip.getId(), "day_1",
                "First record", "Description", "A long body, probably with layout or whatever",
                new Date(), true);
        Record secondRecord = new Record(10001, firstTrip.getId(), "day_2",
                "Second record", "Description", "Another body content", new Date(), true);
        Record thirdRecord = new Record(10002, secondTrip.getId(), "day_3",
                "Third record", "Description", "Record for another trip", new Date(), true);
        recordMapper.addRecord(firstRecord);
        assertEquals(firstRecord, recordMapper.getRecord(firstRecord.getTripId(), firstRecord.getShortcut()));
        recordMapper.addRecord(secondRecord);
        assertEquals(secondRecord, recordMapper.getRecord(secondRecord.getTripId(), secondRecord.getShortcut()));
        recordMapper.addRecord(thirdRecord);
        assertEquals(thirdRecord, recordMapper.getRecord(thirdRecord.getTripId(), thirdRecord.getShortcut()));

        assertEquals("Two records for first trip", 2, recordMapper.getRecordCount(firstTrip.getId()));
        List<Record> firstTripRecords = recordMapper.getRecords(firstTrip.getId(), true);
        assertEquals("Two records for first trip", 2, firstTripRecords.size());
        for (Record r: recordMapper.getRecordsNoBody(firstTrip.getId(), true)) {
            assertNull("Records without a body", r.getBody());
        }
        assertTrue(firstTripRecords.contains(firstRecord) && firstTripRecords.contains(secondRecord));
        assertEquals("One record for second trip", 1, recordMapper.getRecordCount(secondTrip.getId()));
        List<Record> secondTripRecords = recordMapper.getRecords(secondTrip.getId(), true);
        assertTrue(secondTripRecords.contains(thirdRecord));
        
        recordMapper.deleteRecord(firstRecord.getId());
        recordMapper.deleteRecord(secondRecord.getId());
        recordMapper.deleteRecord(thirdRecord.getId());
        tripMapper.deleteTrip(firstTrip.getId());
        tripMapper.deleteTrip(secondTrip.getId());

        List<Trip> trips = tripMapper.getTrips(true);
        assertTrue("Removed first and second trip successfully",
                !trips.contains(firstTrip) && !trips.contains(secondTrip));
    }

}
