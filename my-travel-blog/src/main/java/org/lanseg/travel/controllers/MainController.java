package org.lanseg.travel.controllers;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.lanseg.travel.orm.PictureMapper;
import org.lanseg.travel.orm.PointMapper;
import org.lanseg.travel.orm.RecordMapper;
import org.lanseg.travel.orm.TrackMapper;
import org.lanseg.travel.orm.TripMapper;
import org.lanseg.travel.orm.dao.Picture;
import org.lanseg.travel.orm.dao.Point;
import org.lanseg.travel.orm.dao.Record;
import org.lanseg.travel.orm.dao.Trip;
import org.lanseg.travel.text.Processor;
import org.lanseg.travel.text.TripContext;
import org.lanseg.travel.utils.Utils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.lanseg.travel.utils.Utils.isEditor;
import static org.lanseg.travel.utils.Utils.getCurrentUser;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 *
 * @author lans
 */
@Controller
public class MainController {

    @Resource
    private RecordMapper recordMapper;
    @Resource
    private TripMapper tripMapper;
    @Resource
    private PointMapper pointMapper;
    @Resource
    private PictureMapper pictureMapper;
    @Resource
    private TrackMapper trackMapper;
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("dd.MM.yyyy"), true));
    }

    @RequestMapping("")
    public String index(ModelMap model) {
        model.addAttribute("trips", tripMapper.getTrips(isEditor()));
        model.addAttribute("locale", LocaleContextHolder.getLocale());
        return "main";
    }

    public String prepareModel(ModelMap model, String tripId, String record) {
        boolean editor = isEditor();
        List<Trip> allTrips = tripMapper.getTrips(editor);
        Trip current = allTrips.stream().filter(t -> t.getId().equals(tripId)).findFirst().orElse(null);
        Record currentRecord = record == null ? null : recordMapper.getRecord(tripId, record);
        model.addAttribute("trips", allTrips);
        if (current == null || (record != null && currentRecord == null)
                || (currentRecord != null && currentRecord.isDraft() && !editor)) {
            return "not_available";
        }
        List<Record> records = currentRecord == null
                ? recordMapper.getRecords(current.getId(), editor) : Collections.singletonList(currentRecord);
        List<Record> allHeaders = recordMapper.getRecordsNoBody(tripId, editor);
        List<Picture> pictures = pictureMapper.getTripPictures(tripId, editor);
        List<Point> points = pointMapper.getPoints(tripId, editor);
        Processor p = new Processor(
                new TripContext(current, records, pictures, points), editor
        );
        p.process();
        if (currentRecord != null) {
            int currentIndex = -1;
            for (int i = 0; i < allHeaders.size(); i++) {
                if (allHeaders.get(i).getShortcut().equals(currentRecord.getShortcut())) {
                    currentIndex = i;
                    break;
                }
            }
            if (currentIndex > 0) {
                model.addAttribute("prevRecord", allHeaders.get(currentIndex - 1));
            }
            if (currentIndex < allHeaders.size() - 1) {
                model.addAttribute("nextRecord", allHeaders.get(currentIndex + 1));
            }
        }
        model.addAttribute("points", Utils.groupByKey(points, Point::getRecordId));
        model.addAttribute("pointTypes", pointMapper.getPointTypes());
        model.addAttribute("pictures", Utils.groupByKey(pictures, Picture::getRecordId));
        model.addAttribute("records", records);
        model.addAttribute("currentRecord", currentRecord);
        model.addAttribute("headers", allHeaders);
        model.addAttribute("user", getCurrentUser());
        model.addAttribute("trip", current);
        model.addAttribute("tracks", trackMapper.getTripTracks(tripId));
        model.addAttribute("locale", LocaleContextHolder.getLocale());
        return "trip";
    }

    @RequestMapping("/{trip_id}/{record_shortcut}")
    public String tripRecord(ModelMap model, @PathVariable("trip_id") String tripId,
            @PathVariable("record_shortcut") String recordShortcut) {
        model.addAttribute("allRecords", false);
        return prepareModel(model, tripId, recordShortcut);
    }

    @RequestMapping("/{trip_id}")
    public String trip(ModelMap model, @PathVariable("trip_id") String tripId) {
        model.addAttribute("allRecords", true);
        return prepareModel(model, tripId, null);
    }

    @RequestMapping("/{trip_id}/photos")
    public String photos(ModelMap model, @PathVariable("trip_id") String tripId) {
        Trip trip = tripMapper.getTrip(tripId);
        if (trip == null || (trip.isDraft() && !isEditor())) {
            return "not_available";
        }
        model.addAttribute("pictures", pictureMapper.getTripPictures(tripId, isEditor()));
        model.addAttribute("trip", trip);
        return "photos";
    }
    
    @RequestMapping("/photos")
    public String photos(ModelMap model) {
        model.addAttribute("pictures", pictureMapper.getAllPictures(isEditor()));
        return "photos";
    }
}
